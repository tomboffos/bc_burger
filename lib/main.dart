import 'package:bcburger/FireBase/push_notifications.dart';
import 'package:bcburger/Screens/ForgotPassword.dart';
import 'package:bcburger/Screens/NewPassword.dart';
import 'package:bcburger/Screens/PerceedOrder.dart';
import 'package:bcburger/Screens/ProfilePages/AboutUs.dart';
import 'package:bcburger/Screens/ProfilePages/Confid.dart';
import 'package:bcburger/Screens/ProfilePages/NewsAndSales.dart';
import 'package:bcburger/Screens/ProfilePages/NewsDescription.dart';
import 'package:bcburger/Screens/ProfilePages/ProfileDataScreen.dart';
import 'package:bcburger/Screens/ProfilePages/SupportCall.dart';
import 'package:bcburger/Screens/ProfilePages/SupportCentre.dart';
import 'package:bcburger/Screens/ProfilePages/politics.dart';
import 'package:bcburger/Screens/SecondAuthorization.dart';
import 'package:bcburger/Screens/SplashScreen.dart';
import 'package:bcburger/Screens/authorization.dart';
import 'package:bcburger/Screens/bottomNavBar.dart';
import 'package:bcburger/Screens/korzinaScreen.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/services.dart';
import 'package:bcburger/Screens/registration.dart';
import 'package:bcburger/StateManagement/provide.dart';
import 'package:bcburger/utils/loginProvider.dart';
import 'package:bcburger/utils/productsProvider.dart';
import 'package:flutter/material.dart';

import 'package:provider/provider.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
//
//  Provider.debugCheckInvalidValueType = null;
//  PushNotificationsManager().init();
  runApp(MyApp());
}


class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  String _message = '';

  _registerOnFirebase() async{
    await PushNotificationService().initialize();
  }

  @override
  void initState() {
    _registerOnFirebase();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(value: LoginProvide(),),
        ChangeNotifierProvider.value(value: ProductProvide()),
      ],
      child: Builder(
      builder: (BuildContext context) {
       return ChangeNotifierProvider(
          create: (_) => Provide(),
        child: Builder(
        builder: (BuildContext context) {
          SystemChrome.setPreferredOrientations([
            DeviceOrientation.portraitUp,
            DeviceOrientation.portraitDown,
          ]);
            return MaterialApp(
              builder: (context, child) =>
                  MediaQuery(data: MediaQuery.of(context).copyWith(alwaysUse24HourFormat: true), child: child),
              debugShowCheckedModeBanner: false,
              theme: ThemeData(
                primaryColor: Color(0xffff0000),
                fontFamily: 'OpenSans',
                canvasColor: Colors.white,
                scaffoldBackgroundColor:Colors.white,
                visualDensity: VisualDensity.adaptivePlatformDensity,
              ),
              routes: {
                SecondAuthorizationSceen.id: (context) => SecondAuthorizationSceen(),
                RegisterScreen.id:(context)=> RegisterScreen(),
                BottomNavBar.id: (context) => BottomNavBar(),
                ProfileDataScreen.id : (context)=> ProfileDataScreen(),
                NewsAndSales.id:(context)=>NewsAndSales(),
                NewsDescription.id:(context)=>NewsDescription(),
                SupportCentre.id:(context)=> SupportCentre(),
                SupportCall.id: (context)=> SupportCall(),
                AboutUs.id:(context)=>AboutUs(),
                AuthorizationSceen.id: (context) => AuthorizationSceen(),
                KorzinaScreen.id:(context) => KorzinaScreen(),
                PerceedScreen.id:(context)=> PerceedScreen(),
                ForgotPassword.id: (context)=> ForgotPassword(),
                NewPassword.id:(context)=> NewPassword(),
                DogovorOfety.id: (context) => DogovorOfety(),
                Politics.id:(context)=>Politics(),
              },
              home: SplashScreen(),
            );
        }),
        );
      }
      ),
    );
  }
}

