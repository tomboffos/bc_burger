import 'dart:async';
import 'dart:io';
import 'dart:convert';
import 'dart:math';
import 'package:bcburger/Screens/NewPassword.dart';
import 'package:bcburger/Screens/PerceedOrder.dart';
import 'package:bcburger/models/Adresses.dart';
import 'package:bcburger/models/BusketItems.dart';
import 'package:bcburger/models/Favourite.dart';
import 'package:bcburger/models/FindOrders.dart';
import 'package:bcburger/models/HistoryOrders.dart';
import 'package:bcburger/models/News.dart';
//import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'package:bcburger/models/Order.dart';
import 'package:bcburger/models/user.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:achievement_view/achievement_view.dart';
class LoginProvide extends ChangeNotifier {
 var isLoggedIn = false;
 var isLoading = false;
 var networkProblem = false;
 String email = '';
 var password = '';
 User user;
 var name;
 var tel;
 var password2 = '';
 var raion;
 var adrees;
 var numberKV;
 var podyezd;
 StreamController<HistoryOrders> orderController;
 StreamController<FindOrders> findController;
 StreamController<News> newsController;
 List<DropdownMenuItem<String>> adresses;
 List<String> adressesArray = [];
 List<int> adressPrices = [];
 List<int> minPrices = [];
 Favourite favourite;
 String userInfoText = '';
 Favourite allFavItems = Favourite(data: [],success: false,message: 'В избранных нет товаров.');
 LoginProvide() {
  orderController = StreamController.broadcast();
  findController = StreamController.broadcast();
  newsController = StreamController.broadcast();
  getAdresses();
  stateUser();
  getTimeAboutUs();
  notifyListeners();
 }
 BusketItems provideBusketItems = BusketItems(data: BusketStructure(basket: []),success: false,message: '');
 Future stateUser() async {
  user = await getUser();
  if (user != null) {
   SharedPreferences shared = await SharedPreferences.getInstance();
   userInfoText = shared.getString('user') ?? '';
   provideBusketItems = await currentBusket(user.token);
   allFavItems = await currentFavourite(user.token);
   sendPlayerID();
  }
  notifyListeners();
 }

 Future sendPlayerID()async{
   SharedPreferences shared = await SharedPreferences.getInstance();
   var isSent = shared.getBool('playerid') ?? false;
   if(!isSent){
     final prefs =await SharedPreferences.getInstance();
     final token_firebase = await prefs.getString('token_firebase');
     final respCategory = await http.post(
         'https://burger.a-lux.dev/api/push/token', headers: {
       'Accept': 'application/json',
       'Authorization': 'Bearer ${user.token}',
     }, body: {
       'token': 'wdwdw',
     }).catchError((e) => print(e));
     if(respCategory.statusCode == 200){
       await shared.setBool('playerid', true);
     }
   }
 }
 int currentOrderPrice = 800;
 int curentMinPrice = 2000;
 void changeOrderPrice(index){
  currentOrderPrice =  adressPrices[index];
  curentMinPrice = adressPrices[index];
  notifyListeners();
 }
 Future getAdresses() async {
   try {
     final respCategory = await http.post(
         'https://burger.a-lux.dev/api/address').catchError((e) => print(e));
     var jsonResp = json.decode(respCategory.body);
     if(respCategory.statusCode == 200){
       var addresses = Adresses.fromJson(jsonResp);
       if(addresses.success){
          addresses.data.forEach((element) {
           adressesArray.add(element.name);
           adressPrices.add(element.orderPrice);
           minPrices.add(element.minPrice);
         });
          raion = adressesArray[0];
          currentOrderPrice = adressPrices[0];
          notifyListeners();
       }
     }
   } catch (e) {
     print(e);
   }
   notifyListeners();
 }
 void chengeRaion(val){
   raion = val;
   notifyListeners();
 }
 void chengeRaionInProfile(val){
   newDataForUpdate['district'] = val;
   notifyListeners();
 }
 void showLoseConnection(BuildContext context, mainText, subText, error) {
  AchievementView(context,
      title: mainText,
      subTitle: subText,
      //onTab: _onTabAchievement,
      icon: Icon(
       error ? Icons.error : Icons.check,
       color: Theme
           .of(context)
           .primaryColor,
      ),
      color: Colors.white,
      textStyleTitle: TextStyle(color: Theme
          .of(context)
          .primaryColor),
      textStyleSubTitle: TextStyle(fontSize: 12, color: Theme
          .of(context)
          .primaryColor),
      duration: Duration(seconds: 1),
      isCircle: true,
      listener: (status) {})
   ..show();
 }
 Map<String, dynamic> deliverMap = {
  'delivery': 'Доставка',
 };
 void changeDeliverMap(key,value){
   deliverMap[key] = value;
   notifyListeners();

 }
 Map<String, dynamic> notDeliverMap = {
  'address': 'Ул. Мухита 78 Б',
  'deliver': 'Самовывоз',
  'payment': 'Картой курьеру',
  'delivery_time': 'Как можно скорее',
  'delivery': 'Самовывоз',
 };
 void changeNotDeliverMap(key,value){
  notDeliverMap[key] = value;
  notifyListeners();
 }
 Future getFavFloat()async{
   allFavItems = await currentFavourite(user.token);
  notifyListeners();
 }

 Future changeValue(token, newVal, id) async {
  try {
   final respCategory = await http.post(
       'https://burger.a-lux.dev/api/basket/amount', headers: {
    'Accept': 'application/json',
    'Authorization': 'Bearer $token',
   }, body: {
    'basket_id': id.toString(),
    'amount': newVal.toString(),
   }).catchError((e) => print(e));
   var jsonResp = json.decode(respCategory.body);
   if (jsonResp['success']) {

   }
  } catch (e) {
   print(e);
  }
 }
 void changeValueBasketLocal(increment, id, token)async{
  if(increment == true){
   provideBusketItems.data.basket.singleWhere((element) => element.basketId == id).amount ++;

  }else{
   if(provideBusketItems.data.basket.singleWhere((element) => element.basketId == id).amount > 1){
    provideBusketItems.data.basket.singleWhere((element) => element.basketId == id).amount --;
   }
  }

  int priceAll = 0;


  provideBusketItems.data.basket.forEach((e) {
   priceAll += (int.parse(e.price)*e.amount);


  });

  provideBusketItems.data.allPrice = priceAll;

  provideBusketItems.data.basket.forEach((element) {print(element.amount);});
  notifyListeners();
 }

 int deletingItem = -1;

 Future<BusketItems> deleteItem(token, busketId, context) async {
  try {
   deletingItem = busketId;
   notifyListeners();
   final respCategory = await http.delete(
       'https://burger.a-lux.dev/api/basket/delete/$busketId', headers: {
    'Accept': 'application/json',
    'Authorization': 'Bearer $token',
   }).catchError((e) => print(e));
   var jsonResp = json.decode(respCategory.body);
   if (respCategory.statusCode == 200 && jsonResp['success']) {
    showLoseConnection(context, 'Успешно!', jsonResp['message'], false);
    provideBusketItems = await currentBusket(token);
   }else{
     showLoseConnection(context, 'Ошибка!', jsonResp['message'], true);
   }
  } catch (e) {
   print(e);
  }
  deletingItem = -1;
  notifyListeners();
 }

 bool isClearingBusket = false;

 Future<BusketItems> deleteBusket(token, context) async {
  try {
   isClearingBusket = true;
   notifyListeners();
   final respCategory = await http.delete(
       'https://burger.a-lux.dev/api/basket/clear', headers: {
    'Accept': 'application/json',
    'Authorization': 'Bearer $token',
   }).catchError((e) => print(e));
   var jsonResp = json.decode(respCategory.body);
   if (respCategory.statusCode == 200 && jsonResp['success']) {
    showLoseConnection(context, 'Успешно!', jsonResp['message'], false);
    provideBusketItems = BusketItems(success: false,message: 'Корзина пуста',data: BusketStructure(basket: [],allPrice: 0,));
   } else {
    showLoseConnection(context, 'Ошибка!', jsonResp['message'], true);
   }
  } catch (e) {
   print(e);
  }
  isClearingBusket = false;
  notifyListeners();
 }

 int addingFavItem = -1;

 Future<BusketItems> addFav(token, int id, context) async {
  try {
   addingFavItem = id;
   notifyListeners();
   final respCategory = await http.post(
       'https://burger.a-lux.dev/api/favorites/add', headers: {
    'Accept': 'application/json',
    'Authorization': 'Bearer $token',
   }, body: {
    'id': id.toString()
   }).catchError((e) => print(e));
   var jsonResp = json.decode(respCategory.body);
   if (respCategory.statusCode == 200 && jsonResp['success']) {
    showLoseConnection(context, 'Успешно!', jsonResp['message'], false);
    var newFav = FavouriteItem.fromJson(jsonResp['data']);
    allFavItems.data.add(newFav);
    allFavItems.success = true;
    notifyListeners();
   } else {
    showLoseConnection(context, 'Ошибка!', jsonResp['message'], true);
   }
  } catch (e) {
   print(e);
  }
  addingFavItem = -1;
  notifyListeners();
 }

 Future<BusketItems> deleteFav(token, int id,int justId, context) async {
  try {
   addingFavItem = justId;
   notifyListeners();
   final respCategory = await http.delete(
    'https://burger.a-lux.dev/api/favorites/delete/$id', headers: {
    'Accept': 'application/json',
    'Authorization': 'Bearer $token',
   },).catchError((e) => print(e));
   var jsonResp = json.decode(respCategory.body);
   if (respCategory.statusCode == 200 && jsonResp['success']) {
    showLoseConnection(context, 'Успешно!', jsonResp['message'], false);
    FavouriteItem deletingProduct = allFavItems.data.where((element) => element.favoritId == id).toList()[0];
    allFavItems.data.remove(deletingProduct);
    if(allFavItems.data.length == 0){
    allFavItems = Favourite(data: [],success: false,message: 'В избранных нет товаров');
    }else{
     allFavItems.success = true;
    }
    changeShoildUpdateFavoirute(true);
    notifyListeners();
   } else {
    showLoseConnection(context, 'Ошибка!', jsonResp['message'], true);
   }
  } catch (e) {
   print(e);
  }
  addingFavItem = -1;
  notifyListeners();
 }

 var allTabs = [];

 void changeTabState(value, isOpen) {
  allTabs[value] = isOpen;
  notifyListeners();
 }
 int isAddingItem = -1;
 Future addToBusket(int id,token,context)async{
  try{
   isAddingItem = id;
   notifyListeners();
   var idString = id.toString();
   final respCategory = await http.post('https://burger.a-lux.dev/api/basket/add',
       body: {
        'id': idString,
        'amount':'1',
       },
       headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
       }
   );
   var jsonResp = json.decode(respCategory.body);
   if(jsonResp['success']){
    print(jsonResp);
    print('asdasndada');
    var newProduct = BusketItem.fromJson(jsonResp['data']);
    print(newProduct.price + ' asdjbaskjbdakjsbdkhjasbdks');
    if(provideBusketItems.success == false) {
       provideBusketItems.success = true;
    }
    provideBusketItems.data.basket.add(newProduct);

    print(provideBusketItems.data.basket);
    print('dadasda');

    showLoseConnection(context, 'Успешно!',  jsonResp['message'], false);
   }else{
    showLoseConnection(context, 'Ошибка!',  jsonResp['message'], true);
   }
   isAddingItem = -1;
   notifyListeners();
  }catch(e){
   showLoseConnection(context, 'Ошибка!', 'Проверьте соединение с сетью', true);
   print(e);
   isAddingItem = -1;
  }
  notifyListeners();
 }

 Future<BusketItems> currentBusket(token) async {
  try {
   final respCategory = await http.get(
       'https://burger.a-lux.dev/api/basket/current', headers: {
    'Accept': 'application/json',
    'Authorization': 'Bearer $token',
   }).catchError((e) => print(e));
   BusketItems busketItems;
   if (respCategory.statusCode == 200) {
    var jsonResp = json.decode(respCategory.body);
    var currentBaskets = [];
    int priceAll = 0;
    currentBaskets = provideBusketItems.data.basket;

    provideBusketItems.data.basket.forEach((e) {
     priceAll += (int.parse(e.price)*e.amount);


    });
    busketItems = BusketItems.fromJson(jsonResp);
    if(busketItems.data.basket.length == 0){
    busketItems = BusketItems(data: BusketStructure(basket: []),success: false,message: '');
    }
    provideBusketItems = busketItems;
//    if(cart == true){
//     print(priceAll);
//     provideBusketItems.data.allPrice = priceAll;
//     provideBusketItems.data.basket = currentBaskets;
//     busketItems.data.allPrice = priceAll;
//     busketItems.data.basket = currentBaskets;
//
//    }


    notifyListeners();
    return busketItems;
   } else if (respCategory.statusCode == 404) {
    busketItems = BusketItems(data: BusketStructure(basket: []),success: false,message: 'Корзина пуста');
    provideBusketItems = busketItems;
    notifyListeners();
    return busketItems;
   }
  } catch (e) {
   print(e);
  }
  notifyListeners();
 }

 Future<HistoryOrders> currentHitory(token) async {
  try {
   final respCategory = await http.get(
       'https://burger.a-lux.dev/api/orders/history', headers: {
    'Accept': 'application/json',
    'Authorization': 'Bearer $token',
   }).catchError((e) => print(e));

   HistoryOrders HistoryName;
   if (respCategory.statusCode == 200) {
    // print('did it');
    var jsonResp = json.decode(respCategory.body);
    // print(jsonResp);

    HistoryName = HistoryOrders.fromJson(jsonResp);

    for (var i = 0; i < HistoryName.data.length; i++) {
     allTabs.add(false);
    }


    orderController.add(HistoryName);

    return HistoryName;
   } else if (respCategory.statusCode == 404) {
    var jsonResp = json.decode(respCategory.body);
    HistoryName = HistoryOrders.fromJson(jsonResp);
    orderController.add(HistoryName);
    return HistoryName;
   }
  } catch (e) {
   print(e);
  }
  notifyListeners();
 }
 bool ordering = false;
 Future<HistoryOrders> reOrder(token,orderID,context) async {
  try {
   ordering = true;
   notifyListeners();
   final respCategory = await http.get(
       'https://burger.a-lux.dev/api/orders/history/repeat/$orderID', headers: {
    'Accept': 'application/json',
    'Authorization': 'Bearer $token',
   }).catchError((e) => print(e));
   if (respCategory.statusCode == 200) {
    var jsonResp = json.decode(respCategory.body);
    if(jsonResp['success']){
     showLoseConnection(context, 'Успешно!', jsonResp['message'], false);
    }else{
     showLoseConnection(context, 'Ошибка!', jsonResp['message'], true);
    }
   } else if (respCategory.statusCode == 404) {
    showLoseConnection(context, 'Ошибка!', 'Прверьте соединение с интернетом', true);
   }
  } catch (e) {
   print(e);
  }
  ordering = false;
  notifyListeners();
 }
bool shouldUpdate = true;
 void changeShoildUpdateFavoirute(val){
  shouldUpdate = val;
  notifyListeners();
 }
 Future<Favourite> currentFavourite(token) async {
  Favourite favouriteItem;
  try {
   final respCategory = await http.get(
        'https://burger.a-lux.dev/api/favorites/current', headers: {
     'Accept': 'application/json',
     'Authorization': 'Bearer $token',
    }).catchError((e) => print(e));
    if (respCategory.statusCode == 200) {
     var jsonResp = json.decode(respCategory.body);
     if(!jsonResp['success']){
      favouriteItem = Favourite(data: [],success: false,message: 'В избранных нет товаров.');
     }else{
      favouriteItem = Favourite.fromJson(jsonResp);
     }
    } else if (respCategory.statusCode == 404) {
     favouriteItem = Favourite(success: false,data: [],message: 'В избранных нет товаров.');
    }
    notifyListeners();
  } catch (e) {
   print(e);
  }
  notifyListeners();
  return favouriteItem;
 }
bool isOrdering = false;
 Future<Order> order(token, context,isDeliver) async {
  try {
   isOrdering = true;
   notifyListeners();
   print('12345678--------------');
   print(deliverMap.toString());
   if(isDeliver){
    this.changeDeliverMap('email', this.user.email);
    this.changeDeliverMap('name', this.user.name);
    this.changeDeliverMap('phone', this.user.tel);
    this.changeDeliverMap('delivery_price', this.currentOrderPrice.toString());

   }else {
    this.changeNotDeliverMap('email', this.user.email);
    this.changeNotDeliverMap('name', this.user.name);
    this.changeNotDeliverMap('phone', this.user.tel);
    //this.changeNotDeliverMap('delivery_price', this.currentOrderPrice.toString());
   }


   final respCategory = await http.post(

       'https://burger.a-lux.dev/api/orders/new', headers: {
    'Accept': 'application/json',
    'Authorization': 'Bearer $token',
   },body: isDeliver ? deliverMap : notDeliverMap).catchError((e) => print(e));
   print(isDeliver ? deliverMap : notDeliverMap);
   var jsonResp = json.decode(respCategory.body);

   Order order = Order.fromJson(jsonResp);
   if (respCategory.statusCode == 200) {
    var jsonResp = json.decode(respCategory.body);
    order = Order.fromJson(jsonResp);
    showLoseConnection(context, 'Успешно!', order.message.toString(), false);
    provideBusketItems = await currentBusket(token);
    Navigator.pop(context);
    isOrdering = false;
    return order;
   } else if (respCategory.statusCode == 404) {
    var jsonResp = json.decode(respCategory.body);
    order = Order.fromJson(jsonResp);
    showLoseConnection(context, 'Ошибка!', order.message.toString(), true);
    isOrdering = false;
    return order;
   }
  } catch (e) {
   print(e);
  }
  isOrdering = false;
  notifyListeners();
 }

 Future findOrder(token, context, query) async {
  try {
   print(query);
   final respCategory = await http.post(
       'https://burger.a-lux.dev/api/products/search', headers: {
    'Accept': 'application/json',
    'Authorization': 'Bearer $token',
   }, body: {
    'search': query.toString(),
   }).catchError((e) => print(e));
   var jsonResp = json.decode(respCategory.body);
   FindOrders findOrders = FindOrders.fromJson(jsonResp);
   if (respCategory.statusCode == 200) {
    findController.add(findOrders);
   } else if (respCategory.statusCode == 404) {
    print(findOrders.data.toString());
    findController.add(findOrders);
   }
  } catch (e) {
   print(e);
  }
  notifyListeners();
 }
 Future findNews() async {
   try {
     final respCategory = await http.get(
         'https://burger.a-lux.dev/api/news/catalog',).catchError((e) => print(e));
     var jsonResp = json.decode(respCategory.body);
     News news = News.fromJson(jsonResp);
     if (respCategory.statusCode == 200) {
       newsController.add(news);
     } else if (respCategory.statusCode == 404) {
       newsController.add(news);
     }
   } catch (e) {
     print(e);
   }
   notifyListeners();
 }

 Future exit() async {
  SharedPreferences shared = await SharedPreferences.getInstance();
  shared.setString('user', '');
  saveUser('');
  stateUser();
  notifyListeners();
 }

 Future<User> getUser() async {
  SharedPreferences shared = await SharedPreferences.getInstance();
  var userAr = shared.getString('user') ?? '';
  UserObj userObj;
  if (userAr != '' && userAr != null) {
    var newJson = json.decode(userAr);
    userObj = UserObj.fromJson(newJson);
    user = userObj.data;
   print(user);
  }else{
   user = User(token: '');
   return user;
  }
  notifyListeners();
  return user;
 }

 void startLoading() {
  isLoading = true;
  notifyListeners();
 }

 Future<dynamic> login(context) async {
  try {
   isLoading = true;
   final response = await http.post('https://burger.a-lux.dev/api/login', body: {
    'login': email,
    'password': password,
   });
   var jsonData = json.decode(response.body);
   if (jsonData['success'] == true) {
    await saveUser(response.body);
    allFavItems = await currentFavourite(user.token);
    currentBusket(user.token);
    Navigator.pop(context);
   } else {
    showLoseConnection(context, 'Ошибка', jsonData['message'], true);
   }
  } catch (e) {
   showLoseConnection(context, 'Ошибка','Проверьте соединение с интернетом', true);
   print(e);
  }
  isLoading = false;
  notifyListeners();
 }

 Future<dynamic> register(context) async {
  try {
   print(email);
   isLoading = true;
   final response = await http.post('https://burger.a-lux.dev/api/register', body: {
    'email': email,
    'password': password,
    'password_confirmation': password2.toString(),
    'tel': tel.toString(),
    'name': name.toString(),
    'apartment': numberKV.toString(),
    'address': adrees.toString(),
    'district': raion.toString(),
    'entrance': podyezd.toString(),
   });
   var jsonData = json.decode(response.body);
    if (jsonData['success'] == true) {
    await saveUser(response.body);
    allFavItems = await currentFavourite(user.token);
    Navigator.of(context).popUntil((route) => route.isFirst);
   } else {
    showLoseConnection(context, 'Ошибка', jsonData['message'], true);
   }
  } catch (e) {
   showLoseConnection(context, 'Ошибка','Проверьте соединение с интернетом', true);
  }
  isLoading = false;
  notifyListeners();
 }

 Map<String, dynamic> newDataForUpdate = {};

 Future<dynamic>
 updateUser(context, token) async {
  try {

   isLoading = true;
   notifyListeners();
   print(newDataForUpdate);
   final response = await http.post(
    'https://burger.a-lux.dev/api/user/edit', body: newDataForUpdate, headers: {
    'Accept': 'application/json',
    'Authorization': 'Bearer $token',
   },);
   var jsonData = json.decode(response.body);
   if (jsonData['success'] == true) {
    print(user.toMyJson());
   var initialMap = user.toMyJson();
   initialMap.forEach((key, value) {
    if(newDataForUpdate[key] != null){
     initialMap[key] = newDataForUpdate[key];
    }
   });
   String newSTR = json.encode(initialMap);
   var jsonResp = json.decode(newSTR);
   User newUser =  User.fromJson(jsonResp);
   SharedPreferences shared = await SharedPreferences.getInstance();
   var prevString = shared.getString('user');
   var prevJson = json.decode(prevString);
   UserObj newUserObj = UserObj.fromJson(prevJson);
   newUserObj.data = newUser;
   var saveMap = newUserObj.toJson();
   var saveString = json.encode(saveMap);
   saveUser(saveString);
    showLoseConnection(context, 'Успешно', jsonData['message'], false);

   } else {
    showLoseConnection(context, 'Ошибка', jsonData['message'], true);
   }
  } catch (e) {
   print(e);
  }
  isLoading = false;
  notifyListeners();
 }

 Future saveUser( jsonData) async {
  SharedPreferences shared = await SharedPreferences.getInstance();
  await shared.setString('user', jsonData.toString());
  user = await getUser();
  notifyListeners();
 }

 Future updateLocalUser(Map<String, dynamic> newMap) async {
  SharedPreferences shared = await SharedPreferences.getInstance();
 }


 var forgotLogin = '';
 var forgotLoading = false;
  Future<dynamic> sendEmail(context) async {
   try {
    forgotLoading = true;
    notifyListeners();
    final response = await http.post(
     'https://burger.a-lux.dev/api/user/password/code', body: {
      'email':forgotLogin,
    },);
    var jsonData = json.decode(response.body);
    if (jsonData['success'] == true) {
     Navigator.pushNamed(context, NewPassword.id);
     showLoseConnection(context, 'Успешно', jsonData['message'], false);
    } else {
     showLoseConnection(context, 'Ошибка', jsonData['message'], true);
    }
   } catch (e) {
    print(e);
   }
   forgotLoading = false;
   notifyListeners();
  }
  Map<String, dynamic> newPassword = {};
 Future<dynamic> resetPassword(context) async {
  try {
   forgotLoading = true;
   notifyListeners();
   final response = await http.post(
    'https://burger.a-lux.dev/api/user/password/reset', body: newPassword);
   var jsonData = json.decode(response.body);
   if (jsonData['success'] == true) {
    Navigator.of(context).popUntil((route) => route.isFirst);
    showLoseConnection(context, 'Успешно', 'Пароль изменен', false);
   } else {
    showLoseConnection(context, 'Ошибка', jsonData['message'], true);
   }
  } catch (e) {
   print(e);
  }
  forgotLoading = false;
  notifyListeners();
 }
 Map<String, dynamic> supportMap = {
  'whom':'Администратор заведения',
 };
 void changeWhom(key,val){
  supportMap[key] = val;
  notifyListeners();
 }
 var supportBool = false;
 Future<dynamic> getSupport(context) async {
  try {
   supportBool = true;
   notifyListeners();
   final response = await http.post(
       'https://burger.a-lux.dev/api/message', body: supportMap);
   var jsonData = json.decode(response.body);
   if (jsonData['success'] == true) {
    Navigator.pop(context);
    showLoseConnection(context, 'Успешно', 'Заявка приянта', false);
   } else {
    showLoseConnection(context, 'Ошибка', 'Попробуйте позже', true);
   }
  } catch (e) {
   print(e);
  }
  supportBool = false;
  notifyListeners();
 }

 Map<String, dynamic> callMap = {
  'whom':'Администратор заведения',
 };

 void callChange(key,val){
  callMap[key] = val;
  notifyListeners();
 }
 var supportCallBool = false;
 Future<dynamic> getCall(context) async {
  try {
   supportCallBool = true;
   notifyListeners();
   final response = await http.post(
       'https://burger.a-lux.dev/api/message', body: callMap);
   var jsonData = json.decode(response.body);
   if (jsonData['success'] == true) {
    Navigator.pop(context);
    showLoseConnection(context, 'Успешно', 'Заявка приянта', false);
   } else {
    showLoseConnection(context, 'Ошибка', 'Попробуйте позже', true);
   }
  } catch (e) {
   print(e);
  }
  supportCallBool = false;
  notifyListeners();
 }
 bool timeLoad = false;
 String open='11:00';
 String close='18:00';
 Future<dynamic> getTimeAboutUs() async {
  try {
   timeLoad = true;
   notifyListeners();
   final response = await http.get(
    'https://burger.a-lux.dev/api/get-time',);
   var jsonData = json.decode(response.body);
   if (jsonData['success'] == true) {
     open =  (jsonData['data']['time']['from']);
     close = (jsonData['data']['time']['to']);
   }
  } catch (e) {
   print(e);
  }
  timeLoad = false;
  notifyListeners();
 }

 bool timeGot = false;
 bool isOpen=false;
 Future<dynamic> getTime(context) async {
  timeLoad = true;
  notifyListeners();
  if(!timeGot){
   try {
    final response = await http.get(
     'https://burger.a-lux.dev/api/get-time',);
    var jsonData = json.decode(response.body);
    if (jsonData['success'] == true) {
     if(jsonData['data']['open'] == true){
      isOpen = true;
      open =  (jsonData['data']['time']['from']);
      close = (jsonData['data']['time']['to']);
      timeGot = true;
      print(open);
      Navigator.pushNamed(context, PerceedScreen.id);
     }else{
      showLoseConnection(context, 'Мы закрыты', jsonData['message'].toString(), true);
     }
    } else {
     showLoseConnection(context, 'Ошибка', 'Попробуйте позже', true);
    }
   } catch (e) {
    print(e);
   }
  }else{
   if(isOpen){
    Navigator.pushNamed(context, PerceedScreen.id);
   }else{
    showLoseConnection(context, 'Мы закрыты','', true);
   }
  }
  timeLoad = false;
  notifyListeners();
 }
 }


