import 'dart:async';
import 'dart:convert';
import 'package:bcburger/Screens/MenuItemPage.dart';
import 'package:bcburger/models/BusketItems.dart';
import 'package:bcburger/models/Categories.dart';
import 'package:bcburger/models/Products.dart';
import 'package:bcburger/models/TopProducts.dart';
import 'package:bcburger/models/user.dart';
import 'package:bcburger/utils/loginProvider.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:achievement_view/achievement_view.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
class ProductProvide extends ChangeNotifier {
  ProductProvide(){
    getTop();
  }
  Future getTop()async{
//    Provider.of<LoginProvide>(context).currentFavourite(Provider.of<LoginProvide>(context).user.token);
    topProductsProvide = await fetchTopProducts();

  }
  TopProducts topProductsProvide;
  Categories categoriesProvide;
  Products productsProvide;
  var isFirst = true;
  Future<TopProducts> fetchTopProducts()async{
    try{
      if(isFirst == false){
        print('fetchin fav from top');
        return topProductsProvide;
      }
      if(isFirst){
        print('hope not to see you');
        final response = await http.get('https://burger.a-lux.dev/api/products/top');
        var jsonData = json.decode(response.body);
        TopProducts topProducts;
        if(jsonData['success']){
          topProducts = TopProducts.fromJson(jsonData);
          topProductsProvide = topProducts;
          isFirst = false;
        }else{
        }
      }

      notifyListeners();
      return topProductsProvide;
    }catch(e){
      print(e);
    return TopProducts(data: []);
    }

  }
  void showLoseConnection(BuildContext context, mainText, subText, error) {
    AchievementView(context,
        title: mainText,
        subTitle: subText,
        //onTab: _onTabAchievement,
        icon: Icon(
          error ? Icons.error : Icons.check,
          color: Theme.of(context).primaryColor,
        ),
        color: Colors.white,
        textStyleTitle: TextStyle(color: Theme.of(context).primaryColor ),
        textStyleSubTitle: TextStyle(fontSize:12,color:  Theme.of(context).primaryColor ),
        duration: Duration(seconds: 1),
        isCircle: true,
        listener: (status) {})
      ..show();
  }
  int selectedItem = -1;
  void goToMenuItem({value,context,isInMenu, Categories categories}){
    selectedItem = value;
    if((isInMenu?? false) == false){
      Navigator.push(context,MaterialPageRoute(builder: (context)=> MenuItemPage()));
    }else{
      fetchProductsinCat(value);
      Navigator.push(context,MaterialPageRoute(builder: (context)=> MenuItemPage()));
    }

    notifyListeners();
  }
  var isFirstDrawer = true;
  Future<Categories> fetchCategories()async{
    try{
      if(isFirstDrawer){
        final respCategory = await http.get('https://burger.a-lux.dev/api/catalogs');
        var jsonResp = json.decode(respCategory.body);
        print(jsonResp);
        Categories categories;
        if(jsonResp['success']){
          categories = Categories.fromJson(jsonResp);
//          categories.data.sort((a,b) => a.id.compareTo(b.id));
          categoriesProvide = categories;
          isFirstDrawer = false;
        }else{}
        notifyListeners();
        return categoriesProvide;
      }else{
        return categoriesProvide;
      }
    }catch(e){}
    return categoriesProvide;
  }
  Map<String, Products> mapOfDownCats = {};
  Map<int,bool> mapOfLoadCats = {};
  int isProductLoading = -1;
  void changeSelectedItem(id){
    selectedItem = id;
    notifyListeners();
  }
  Future<Products> fetchProductsinCat(id)async{
    print(id.toString() + ' prom provider');
    try{
      if(mapOfDownCats.containsKey(id.toString()) ?? false){
        productsProvide = mapOfDownCats[id.toString()];
      }else{
        print(id);
        isProductLoading = id;
        final respCategory = await http.get('https://burger.a-lux.dev/api/catalogs/$id/products');
        var jsonResp = json.decode(respCategory.body);
        Products products;
        if(jsonResp['success']){
          products = Products.fromJson(jsonResp);
          productsProvide = products;
          mapOfDownCats[id.toString()] = products;
        }else{

        }
        isProductLoading = -1;
      }
      notifyListeners();
      return productsProvide;
    }catch(e){
      print(e);
    }
   return productsProvide;
  }

}
