import 'package:bcburger/Screens/MenuItemPage.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:bcburger/NotNeccesaryData.dart';
class Provide extends ChangeNotifier {
var isWithMe = 0;
void changeIsWithMe(value){
  isWithMe = value;
  notifyListeners();
}
var cashType = 0;
void changeCashType(value){
  cashType = value;
  notifyListeners();
}
var shipPoints = 0;
var allTabs = [true,false];
void changeTabState(value,isOpen){
  allTabs[value] = isOpen;
  notifyListeners();
}

void changeShipPoint(value){
  shipPoints = value;
  notifyListeners();
}
}