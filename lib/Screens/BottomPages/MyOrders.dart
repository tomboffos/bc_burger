import 'package:bcburger/StateManagement/provide.dart';
import 'package:bcburger/Widgets/LoadingCircle.dart';
import 'package:bcburger/Widgets/OrangeCircle.dart';
import 'package:bcburger/Widgets/SecondaryContainer.dart';
import 'package:bcburger/models/HistoryOrders.dart';
import 'package:bcburger/utils/loginProvider.dart';
import 'package:flutter/material.dart';
import 'package:bcburger/Screens/ProfilePages/AboutUs.dart';
import 'package:bcburger/Screens/ProfilePages/NewsAndSales.dart';
import 'package:bcburger/Screens/ProfilePages/ProfileDataScreen.dart';
import 'package:bcburger/Screens/ProfilePages/SupportCall.dart';
import 'package:bcburger/Screens/ProfilePages/SupportCentre.dart';
import 'package:bcburger/Screens/authorization.dart';
import 'package:provider/provider.dart';

class MyOrders extends StatefulWidget {
  @override
  _MyOrdersState createState() => _MyOrdersState();
}

class _MyOrdersState extends State<MyOrders> {
  var dynamicMap;
  HistoryOrders orders;
  @override
  void initState() {
    // TODO: implement initState
    var token = Provider.of<LoginProvide>(context,listen: false).user.token;
    Provider.of<LoginProvide>(context,listen: false).currentHitory(token);

    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Мои заказы',
          style: TextStyle(
            color: Colors.white,
            fontSize: 18,
            fontWeight: FontWeight.w700,
          ),
        ),
      ),
      body: StreamBuilder<HistoryOrders>(
        stream: Provider.of<LoginProvide>(context).orderController.stream,
        builder: (context, snapshot) {


          return
          snapshot.data != null && (snapshot.data?.success ?? true) ? Container(
            padding: EdgeInsets.all(15.0),
            child:
            ListView.builder(itemBuilder: (context,i){
              return CardList(context, i,snapshot.data.data[i]);
            },itemCount: snapshot.data.data.length,)
          ) : snapshot.data != null && (snapshot.data?.success ?? false) == false ? Center(child: Text(snapshot.data?.message),) : OrangeCircle();
        }


      ),
    );
  }
}
Future<List<Widget>> getAllWidgets(List<ProductsHistory> products)async{
 List<Widget> listTiles = [];
 products.forEach((element) {
   var newTile =  ListTile(
     title: Padding(
       padding: const EdgeInsets.only(left: 16),
       child: Text(element.name,style: TextStyle(
           color: Colors.black,
           fontWeight: FontWeight.w600,
           fontSize: 16
       ),),
     ),
     subtitle: Padding(
       padding: const EdgeInsets.only(left: 16),
       child: Text('№'+ element.id.toString(),style: TextStyle(
           color: Color(0xff4B4B4B),
           fontWeight: FontWeight.normal,
           fontSize: 14
       ),),
     ),
     trailing: Column(
       crossAxisAlignment: CrossAxisAlignment.end,
       mainAxisAlignment: MainAxisAlignment.center,
       children: [
         Text(element.amount.toString() + (element.amount == 1 ? ' порция' : ' порции'),style: TextStyle(
             color: Colors.black,
             fontWeight: FontWeight.w500,
             fontSize: 14
         ),),
         Text(element.allPrice.toString()+ ' тг.',style: TextStyle(
             color: Colors.black,
             fontWeight: FontWeight.w600,
             fontSize: 14
         ),),
       ],
     ),
   );
   listTiles.add(newTile);
 });
 return listTiles;
}

Widget CardList(context,main, History history) {
  var provide =Provider.of<LoginProvide>(context);
  var w = MediaQuery.of(context).size.width;
  return SingleChildScrollView(
    child: Card(
      child: FutureProvider<List<Widget>>(
        create: (context) => getAllWidgets(history.products),
        child: Consumer<List<Widget>>(
          builder: (context,snap,child){

            return snap != null ? ExpansionTile(
              onExpansionChanged: (val){
                provide.changeTabState(main,val);
              },
              title: Text('Заказ №' + history.orderNumber,style: TextStyle(
                  color: Theme.of(context).primaryColor,
                  fontWeight: FontWeight.w600,
                  fontSize: 16
              ),),
              subtitle: Text(history.createdAt,style: TextStyle(
                  color: Color(0xff4B4B4B),
                  fontWeight: FontWeight.normal,
                  fontSize: 14
              ),),
              trailing: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Text(history.status,style: TextStyle(
                      color: Color(0xff20A03C),
                      fontWeight: FontWeight.w600,
                      fontSize: 14
                  ),),
                  Icon( !provide.allTabs[main] ? Icons.keyboard_arrow_down : Icons.keyboard_arrow_up,color: Theme.of(context).primaryColor,size: 30,)
                ],
              ),
              children: <Widget>[
                ...snap,
                Padding(
                  padding: const EdgeInsets.only(bottom: 0),
                  child: GestureDetector(
                    onTap: (){
                     provide.reOrder(provide.user.token, history.orderNumber, context);
                    },
                    child: Container(width:w,
                      color: Theme.of(context).primaryColor,
                      height: 50,
                      child: Center(
                        child: provide.ordering ? LoadingCircle() : Text(
                          'Заказать',style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w600,
                            fontSize: 16
                        ),
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ) : OrangeCircle();
          },
        ),
      ),
    ),
  );
}

