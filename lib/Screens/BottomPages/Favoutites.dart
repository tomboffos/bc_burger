import 'package:bcburger/Widgets/OrangeCircle.dart';
import 'package:bcburger/Widgets/SecondaryContainer.dart';
import 'package:bcburger/models/Favourite.dart';
import 'package:bcburger/utils/loginProvider.dart';
import 'package:flutter/material.dart';

import 'package:provider/provider.dart';
class FavouriteScreen extends StatefulWidget {
  @override
  _FavouriteScreenState createState() => _FavouriteScreenState();
}

class _FavouriteScreenState extends State<FavouriteScreen> {
  @override
  Widget build(BuildContext context) {
    var w = MediaQuery.of(context).size.width;
    var h = MediaQuery.of(context).size.height;
    var snap = Provider.of<LoginProvide>(context).allFavItems;
    return Scaffold(

      appBar:  AppBar(
        title: Text(
          'Избранное',
          style: TextStyle(
            color: Colors.white,
            fontSize: 18,
            fontWeight: FontWeight.w700,
          ),
        ),
        actions: [GestureDetector(child: IconButton(icon:Icon(Icons.refresh,color: Colors.white,),onPressed: (){
          Provider.of<LoginProvide>(context,listen: false).getFavFloat();
        },),)],
      ),
      body: snap!= null && (snap.success ?? false) ?  Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8,vertical: 16),
            child: Container(
              width: w-16,
              height: h-78,
              child: GridView.builder(gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisSpacing: 8,
                  mainAxisSpacing: 24,
                  childAspectRatio:(w/2-20)/340,
                  crossAxisCount:  2 ), itemBuilder: (context, i){
                return SecondaryContainer(id: snap.data[i].id,name: snap.data[i].title,
                  inFav: true,
                  gram: snap.data[i].edMassa,
                  image: snap.data[i].images,
                  price: snap.data[i].price,
                  content: snap.data[i].content,

                );
              },itemCount: snap.data.length,),
            ),
          ) : snap!= null && snap.success == false ? Center(child: Text(snap.message),) : OrangeCircle(),
    );
  }
}
