import 'package:bcburger/Screens/ProfilePages/AboutUs.dart';
import 'package:bcburger/Screens/ProfilePages/Confid.dart';
import 'package:bcburger/Screens/ProfilePages/NewsAndSales.dart';
import 'package:bcburger/Screens/ProfilePages/ProfileDataScreen.dart';
import 'package:bcburger/Screens/ProfilePages/SupportCall.dart';
import 'package:bcburger/Screens/ProfilePages/SupportCentre.dart';
import 'package:bcburger/Screens/ProfilePages/politics.dart';
import 'package:bcburger/Screens/authorization.dart';
import 'package:bcburger/utils/loginProvider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
class ProfileScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var w = MediaQuery.of(context).size.width;
    var h = MediaQuery.of(context).size.height;
    var loginProvide = Provider.of<LoginProvide>(context);
    var headings = ['Личные данные','Новости и акции','Служба поддержки','Обратный звонок','О нас','Договоры оферты','Политика конфиденциальности','Выход'];
    var allPages = [ProfileDataScreen.id,NewsAndSales.id,SupportCentre.id,SupportCall.id,AboutUs.id,DogovorOfety.id,Politics.id,AuthorizationSceen.id,];
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: Icon(Icons.person,color: Color(0xff1A1A1A),size: 30,),
        title: Text('Профиль',style: TextStyle(
          color: Color(0xff1A1A1A),
          fontSize: 18,
          fontWeight: FontWeight.w700,
        ),),
      ),
      body: ListView.builder(itemBuilder: (context,i){
        return Container(
          decoration:i!=7 ? BoxDecoration(
            border: Border(bottom: BorderSide(color: Color(0xffC4C4C4),width: 1)),
          ) : BoxDecoration(),
          child: ListTile(
            onTap: (){
              if( i == 7){
                loginProvide.exit();
              }else{
                Navigator.pushNamed(context, allPages[i]);
              }
            },
            title: Text(headings[i],style: i!= 7 ? TextStyle(
              color: Color(0xff1A1A1A),
              fontWeight: FontWeight.w600,
              fontSize: 16,
            ) : TextStyle(
              color: Theme.of(context).primaryColor,
              fontSize: 16,
              fontWeight: FontWeight.w600,
            ),),
          ),
        );
      },itemCount: headings.length,),
    );
  }
}
