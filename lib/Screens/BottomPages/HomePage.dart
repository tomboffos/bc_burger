import 'dart:math';
import 'dart:ui';
import 'package:bcburger/Screens/ProductPage.dart';
import 'package:bcburger/StateManagement/provide.dart';
import 'package:bcburger/Widgets/LikeButton.dart';
import 'package:bcburger/Widgets/LoadingCircle.dart';
import 'package:bcburger/Widgets/SearchWidget.dart';
import 'package:bcburger/Widgets/busketIcon.dart';
import 'package:bcburger/models/Categories.dart';
import 'package:bcburger/models/FindOrders.dart';
import 'package:bcburger/models/TopProducts.dart';
import 'package:bcburger/utils/loginProvider.dart';
import 'package:shimmer/shimmer.dart';
import 'package:bcburger/utils/productsProvider.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:bcburger/Widgets/SecondaryContainer.dart';
class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    var provide = Provider.of<ProductProvide>(context);
    var w = MediaQuery.of(context).size.width;
    var futureDrawer = Provider.of<ProductProvide>(context,listen: false).fetchCategories();
    var loginProvide = Provider.of<LoginProvide>(context).provideBusketItems?.data?.basket?.length ?? 0;
    var snapshot = provide.topProductsProvide;
    return Scaffold(
        appBar: AppBar(
          title: Row(
            children: [
              Image.asset(
                'assets/bclogo.png',
                width: 30,
              ),
              SizedBox(
                width: 8,
              ),
              Text(
                'B&C Burger',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 18,
                  fontWeight: FontWeight.w700,
                ),
              ),
            ],
          ),
          actions: [
            BusketIcon(loginProvide: loginProvide,),
            SizedBox(
              width: 8,
            ),
            SearchButton(),
            SizedBox(
              width: 16,
            ),
          ],
        ),
        drawer: FutureProvider<Categories>(
          create: (context) =>futureDrawer,
          child: Consumer<Categories>(
              builder: (context,snap,child){
              return Container(
                  width: w,
                  child: Drawer(
                    child: Column(
                      children: [
                        AppBar(
                          leading: Transform.rotate(angle: pi/2, child: GestureDetector(
                              onTap: (){
                                Navigator.pop(context);
                              },
                              child: Icon(Icons.menu,)),),
                          title: Row(
                            children: [
                              Image.asset(
                                'assets/bclogo.png',
                                width: 30,
                              ),
                              SizedBox(
                                width: 8,
                              ),
                              Text(
                                'B&C Burger',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 18,
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                            ],
                          ),
                        ),
                        Expanded(
                          child: Container(
                            child: ListView.builder(itemBuilder: (context,i){
                              return Container(
                                color: provide.selectedItem == snap.data[i].id ? Theme.of(context).primaryColor : Colors.white,
                                child: ListTile(
                                  title: Padding(
                                    padding: const EdgeInsets.only(left: 24),
                                    child: snap != null ? Text(snap.data[i].name,style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.normal,
                                      color: provide.selectedItem == snap.data[i].id? Colors.white  : Colors.black
                                    ),) : Container(
                                      width: w-32,
                                      height: 20.0,
                                      child: Shimmer.fromColors(
                                          baseColor: Colors.grey[300],
                                          highlightColor: Colors.grey[100],
                                        child: Container(
                                          decoration: BoxDecoration(
                                            borderRadius: BorderRadius.circular(8),
                                            color: Colors.white,
                                          ),
                                          width: w-32,
                                          height: 20.0,
                                        )
                                      ),
                                    ),
                                  ),
                                  onTap: (){
                                    provide.goToMenuItem(value: snap.data[i].id,context: context,categories: snap,isInMenu: true);
                                  },
                                ),
                              );
                            },itemCount:snap != null ?  snap.data.length: 9,),
                          ),
                        ),
                      ],
                    ),
                  ),
                );
            }
          ),
        ),
        body: snapshot != null ? Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: ListView.builder(itemBuilder: (context,i){
                Data TopProduct = snapshot.data[i];
                return BlocInfoTop(i: i,topProduct: TopProduct,);
              },itemCount: snapshot.data.length,),
            ) : ShimmerHome(),

    );
  }
}

class ShimmerHome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var w = MediaQuery.of(context).size.width;
    var h = MediaQuery.of(context).size.height;
    return Padding(
      padding: const EdgeInsets.only(top: 24,left: 16,right: 16),
      child: Container(
        width: w-32,
        height: h,
        child: Shimmer.fromColors(
            baseColor: Colors.grey[300],
            highlightColor: Colors.grey[100],
            child: SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        color: Colors.white,
                      ),
                      width: w/3,
                      height: 20.0,
                    ),
                    SizedBox(height: 16,),
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        color: Colors.white,
                      ),
                      width: w-32,
                      height: 230.0,
                    ),
                    SizedBox(height: 32,),
                    Row(children: [
                      Expanded(
                        flex: 1,
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8),
                            color: Colors.white,
                          ),
                          height: 340,
                        ),
                      ),
                      SizedBox(width: 16,)

                      ,                  Expanded(
                        flex: 1,
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8),
                            color: Colors.white,
                          ),
                          width: w/2-20,
                          height: 340,
                        ),
                      ),

                    ],)

                  ],
                ),
              ),
            )
        ),
      ),
    );
  }
}

class BlocInfoTop extends StatelessWidget {
  final i;
  final Data topProduct;
  BlocInfoTop({this.i,this.topProduct});
  List<TopContainer> FullContainers = [
  ];
  List<TopProductsTop> littleContainer = [];
  List<TopContainer> generate(){
   topProduct.products.forEach((element) {
     if(element.fullscreen == 1){
       FullContainers.add(
         TopContainer(image: element.image,gram: element.edMassa,content: element.content,name: element.name,price: element.price,id: element.id,)
       );
     }else{
       littleContainer.add(element);
     }
   });
   return FullContainers;
  }
  @override
  Widget build(BuildContext context) {
    var w = MediaQuery.of(context).size.width;
    var h = MediaQuery.of(context).size.height;
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.only(top: 24, bottom: 16),
          child: Text(
            topProduct.name,
            style: TextStyle(
              color: Theme.of(context).primaryColor,
              fontSize: 20,
            ),
          ),
        ),
        ...generate(),
       if(FullContainers.length != 0) SizedBox(height: 24,),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8,vertical: 16),
          child: Container(
            width: w-16,
            height: 340.0*(littleContainer.length/2).ceil(),
            child: GridView.builder(gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisSpacing: 8,
                mainAxisSpacing: 24,
                childAspectRatio:(w/2-20)/340,
                crossAxisCount:  2 ), itemBuilder: (context, i){
              return SecondaryContainer(id: littleContainer[i].id,price: littleContainer[i].price,image: littleContainer[i].image,name: littleContainer[i].name,
                gram: littleContainer[i].edMassa,content: littleContainer[i].content,
              );
            },itemCount: littleContainer.length,   physics: NeverScrollableScrollPhysics(),
            ),
          ),
        ),
      ],
    );
  }
}

class TopContainer extends StatelessWidget {
  final id;
  final image;
  final name;
  final content;
  final gram;
  final price;
  TopContainer({this.image,this.name,this.content,this.gram,this.price,this.id});
  @override
  Widget build(BuildContext context) {
    var mainTextStyle = TextStyle(
      fontWeight: FontWeight.w600,
      fontSize: 16,
    );
    var secondaryTextStyle = TextStyle(
      color: Color(0xff0D0D0D),
      fontWeight: FontWeight.normal,
      fontSize: 12,
    );
    var w = MediaQuery.of(context).size.width;
    return GestureDetector(
      onTap: (){
        Navigator.push(context, MaterialPageRoute(builder: (context)=> ProductPage(heroId: id,image: image,gram: gram,idItem: id,inFav: true,name: name,price: price,content: content,)));
      },
      child: Container(
        width: w,
        height: 230,
        decoration: (image ?? false) != null ?   BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          image: DecorationImage(
              image:  CachedNetworkImageProvider(image), fit: BoxFit.cover),
        ) : BoxDecoration(  borderRadius: BorderRadius.circular(8),),
        child: Stack(
          children: [
            Positioned(
              top: 0,
              right: 0,
              child: Container(
                width: w/2 + 32,
                height: 230,
                color: Colors.white.withOpacity(0.9),
                child: Column(
                  children: [
                    SizedBox(height: 10,),
                    Text(name ?? '',style: mainTextStyle,),
                    Padding(padding: EdgeInsets.symmetric(horizontal: 16,vertical: 4),child: Container(
                      width: w,
                      height: 1,
                      color: Theme.of(context).primaryColor,
                    ),),
                    Padding(
                      padding:EdgeInsets.symmetric(horizontal: 16,vertical: 4),
                      child: Text(content ?? '',style: secondaryTextStyle,textAlign: TextAlign.center,),
                    ),
                    Expanded(
                      child: Container(
                        child: Column(children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 16,vertical: 8),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(gram ?? '',style: TextStyle(
                                  fontSize: 12,
                                  color: Color(0xff5E5E5E)
                                ),),
                                Text(price ?? '',style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black
                                ),),
                              ],
                            ),
                          ),
                          GestureDetector(
                            onTap: (){
                              var user = Provider.of<LoginProvide>(context,listen: false).user;
                              Provider.of<LoginProvide>(context,listen: false).addToBusket(id,user.token,context);
                            },
                            child: Container(
                              width: w/2+32,
                              height: 50,
                              decoration: BoxDecoration(
                                color: Theme.of(context).primaryColor,
                                borderRadius: BorderRadius.only(bottomRight: Radius.circular(8))
                              ),
                              child: Provider.of<LoginProvide>(context).isAddingItem != id ? Center(child: Text(
                                'В корзину',style: TextStyle(
                                color: Colors.white,
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                              ),
                              ),) : LoadingCircle(),
                            ),
                          ),
                        ],mainAxisAlignment: MainAxisAlignment.end,
                          crossAxisAlignment: CrossAxisAlignment.end,
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
            Positioned(top: 8,left: 8,child: LikeButton(id: id,),)
          ],
        ),
      ),
    );
  }
}

