import 'package:bcburger/Screens/ProfilePages/AboutUs.dart';
import 'package:bcburger/Screens/SecondAuthorization.dart';
import 'package:bcburger/Screens/registration.dart';
import 'package:bcburger/Widgets/ActionButton.dart';
import 'package:bcburger/utils/loginProvider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
class AuthorizationSceen extends StatelessWidget {
  static var id = 'authorization';
  @override
  Widget build(BuildContext context) {
    var h = MediaQuery.of(context).size.height;
    var w = MediaQuery.of(context).size.width;
    return Scaffold(
     body: SafeArea(
       child: Column(
         crossAxisAlignment: CrossAxisAlignment.center,
         mainAxisAlignment: MainAxisAlignment.center,
         children: [
           Flexible(
             flex: 1,
             child: SizedBox(height: h,),
           ),
           Padding(
             padding:  EdgeInsets.symmetric(horizontal: w*0.2),
             child: Image.asset('assets/bclogo.png'),
           ),
           Flexible(
             flex: 1,
             child: SizedBox(height: h,),
           ),
           ActionButton(text: 'Войти',enable: true,onTap: (){
             Navigator.pushNamed(context, SecondAuthorizationSceen.id);
           },),
           SizedBox(height: 16,),
           RegisterButton(onTap: (){
             print(LoginProvide());
             Navigator.pushNamed(context, RegisterScreen.id);
           },),
           Flexible(
             flex: 2,
             child: SizedBox(height: h,),
           ),
           GestureDetector(
               onTap: (){
                 if(Provider.of<LoginProvide>(context,listen: false).adressesArray.length > 0){
                   Navigator.pushNamed(context, AboutUs.id);
                 }
               },
               child: Text('О нас',style: TextStyle(color: Colors.black,fontSize: 16,fontWeight: FontWeight.bold,decoration: TextDecoration.underline),)),
           SizedBox(height: 24,),
         ],
       ),
     ),
    );
  }
}

class RegisterButton extends StatelessWidget {
  final onTap;
  RegisterButton({this.onTap});
  @override
  Widget build(BuildContext context) {
    var w = MediaQuery.of(context).size.width;
    var theme = Theme.of(context);
    return GestureDetector(
      onTap: onTap,

      child: Container(
        width: w-32,
        padding:  EdgeInsets.symmetric(vertical: 16),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          border: Border.all(color: theme.primaryColor),
        ),
        child: Center(
          child: Text(
            'Регистрация',style: TextStyle(color: theme.primaryColor,fontWeight: FontWeight.bold,fontSize: 16),
          ),
        ),

      ),
    );
  }
}
