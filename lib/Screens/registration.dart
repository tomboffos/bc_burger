import 'package:bcburger/Screens/bottomNavBar.dart';
import 'package:bcburger/Widgets/ProfileDataFields.dart';
import 'package:bcburger/utils/loginProvider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
class RegisterScreen extends StatelessWidget {
  static var id = 'register';
  @override
  Widget build(BuildContext context) {
    List<DropdownMenuItem<String>> items = [DropdownMenuItem(
      child: Text('sda'),
      value: 'k' ,
    ),];
    String dropdownValue = items[0].value;
    var h = MediaQuery.of(context).size.height;
    var w = MediaQuery.of(context).size.width;
    var loginProvide = Provider.of<LoginProvide>(context);
    var enable = loginProvide.email.contains('@') && (loginProvide.password2 == loginProvide.password) && loginProvide.podyezd != '' &&loginProvide.numberKV !='' && loginProvide.adrees != '' &&loginProvide.tel != '';
    return Scaffold(
      body: SafeArea(
        child: Stack(
          alignment: Alignment.center,
          children: [
            Positioned(
              top: 16,
              child: Text('Регистрация',style: TextStyle(color: Colors.black,fontSize: 20,fontWeight: FontWeight.bold),),
            ),
            Positioned(
              top: 42,
              child: ProfileDataFields(h: h, w: w, dropdownValue: dropdownValue, items: Provider.of<LoginProvide>(context).adresses,saveOrNot: false,enable: enable,),
            ),
          ],
        ),
      ),
    );
  }
}

