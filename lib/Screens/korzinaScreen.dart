import 'package:bcburger/Screens/PerceedOrder.dart';
import 'package:bcburger/Widgets/ActionButton.dart';
import 'package:bcburger/Widgets/LoadingCircle.dart';
import 'package:bcburger/Widgets/OrangeCircle.dart';
import 'package:bcburger/models/BusketItems.dart';
import 'package:bcburger/utils/loginProvider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';
import 'dart:async';
class KorzinaScreen extends StatefulWidget {
  static var id = 'koriznaScreen';
  @override
  _KorzinaScreenState createState() => _KorzinaScreenState();
}

class _KorzinaScreenState extends State<KorzinaScreen> {
  @override
  void initState() {
    // TODO: implement initState
    var token = Provider.of<LoginProvide>(context, listen: false).user.token;
    Provider.of<LoginProvide>(context, listen: false).currentBusket(token);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var w = MediaQuery.of(context).size.width;
    var snapshot = Provider.of<LoginProvide>(context).provideBusketItems;
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Корзина',
          style: TextStyle(
            color: Colors.white,
            fontSize: 20,
            fontWeight: FontWeight.w700,
          ),
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: GestureDetector(
              onTap: () {
                var token = Provider.of<LoginProvide>(context,
                    listen: false)
                    .user
                    .token;
                Provider.of<LoginProvide>(context, listen: false)
                    .deleteBusket(token, context);
              },
              child:  Provider.of<LoginProvide>(context, listen: false).isClearingBusket == false ? Text(
                'Очистить',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 16,
                  fontWeight: FontWeight.w700,
                ),
              ) : LoadingCircle(),
            ),
          ),
        ],
      ),
      body:  snapshot != null && snapshot.success
                ? snapshot.data.basket.length == 0 ? Center(child: Text('Корзина пуста'),) : Column(
                    children: [
                      SizedBox(
                        height: 16,
                      ),
                      Expanded(
                        child: Container(
                          width: w,
                          child: ListView.builder(
                            itemBuilder: (context, i) {
                              return ShopItem(
                                busketItem: snapshot.data.basket[i],
                              );
                            },
                            itemCount: snapshot.data.basket.length,
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 8),
                        child: ActionButton(
                          enable: true,
                          trigger: Provider.of<LoginProvide>(context,listen: false).timeLoad,
                          text: 'Оформить заказ',
                          onTap: () {
                          Provider.of<LoginProvide>(context,listen: false).getTime(context);
                          },
                        ),
                      )
                    ],
                  )
                : snapshot != null && snapshot.success == false
                    ? Center(
                        child: Text(snapshot.message ?? ''),
                      )
                    : ShimmerBusket(),

    );
  }
}

class ShimmerBusket extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var w = MediaQuery.of(context).size.width;
    var h = MediaQuery.of(context).size.height;
    return Padding(
      padding: const EdgeInsets.only(top: 24, left: 16, right: 16),
      child: Container(
        width: w - 32,
        height: h,
        child: Shimmer.fromColors(
            baseColor: Colors.grey[300],
            highlightColor: Colors.grey[100],
            child: SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    width: w - 32,
                    height: 150,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(8),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.5),
                          spreadRadius: 2,
                          blurRadius: 7,
                          offset: Offset(0, 3), // changes position of shadow
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    width: w - 32,
                    height: 150,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(8),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.5),
                          spreadRadius: 2,
                          blurRadius: 7,
                          offset: Offset(0, 3), // changes position of shadow
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    width: w - 32,
                    height: 150,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(8),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.5),
                          spreadRadius: 2,
                          blurRadius: 7,
                          offset: Offset(0, 3), // changes position of shadow
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                ],
              ),
            )),
      ),
    );
  }
}

class ShopItem extends StatefulWidget {
  final BusketItem busketItem;
  ShopItem({this.busketItem});

  @override
  _ShopItemState createState() => _ShopItemState();
}

class _ShopItemState extends State<ShopItem> {
  var newAmaunt = 1;
  Timer timer;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {
      timer = Timer(Duration(), (){

      });
      newAmaunt = widget.busketItem.amount ?? 1;
    });
  }
  @override
  Widget build(BuildContext context) {
    var mainTextStyle = TextStyle(
      fontWeight: FontWeight.w600,
      fontSize: 14,
    );
    var secondaryTextStyle = TextStyle(
      color: Color(0xff0D0D0D),
      fontWeight: FontWeight.normal,
      fontSize: 12,
    );
    var w = MediaQuery.of(context).size.width;
    var token = Provider.of<LoginProvide>(context).user.token;
    var update = Provider.of<LoginProvide>(context);
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
      child: Container(
        width: w - 32,
        height: 150,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(8),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 2,
              blurRadius: 7,
              offset: Offset(0, 3), // changes position of shadow
            ),
          ],
        ),
        child: Stack(
          alignment: Alignment.center,
          children: [
            Positioned(
              top: 16,
              child: Padding(
                padding: const EdgeInsets.only(right: 16, left: 16),
                child: Container(
                  width: w - 64,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(

                        widget.busketItem.name ?? '',

                        style: mainTextStyle,
                      ),

                      Text((int.parse(widget.busketItem.price) * newAmaunt).toString() ?? '',
                        style: mainTextStyle,
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Positioned(
              top: 44,
              child: Container(
                height: 1,
                color: Theme.of(context).primaryColor,
                width: w - 64,
              ),
            ),
            Positioned(
              left: 16,
              top: 52,
              child: Container(
                height: 76,
                width: 96,
                decoration: BoxDecoration(
                  image: DecorationImage(
                      image:   widget.busketItem.images == null  ?  AssetImage('assets/dishes.png'): NetworkImage(widget.busketItem.images != null ? widget.busketItem.images : '') ,
                      fit: BoxFit.fitHeight),
                ),
              ),
            ),
            Positioned(
              top: 52,
              right: 16,
              child: Text(
                widget.busketItem.edMassa ?? '',
                style: secondaryTextStyle,
              ),
            ),
            Positioned(
              right: 16,
              bottom: 24,
              child: GestureDetector(
                onTap: () {
                  var token = Provider.of<LoginProvide>(context, listen: false)
                      .user
                      .token;
                  Provider.of<LoginProvide>(context, listen: false)
                      .deleteItem(token, widget.busketItem.basketId, context);
                },
                child: Provider.of<LoginProvide>(context, listen: false)
                            .deletingItem ==
                        widget.busketItem.basketId
                    ? OrangeCircle()
                    : Icon(
                        Icons.delete,
                      ),
              ),
            ),
            Positioned(
              bottom: 24,
              left: 128,
              child: Row(
                children: [
                  GestureDetector(
                    onTap: (){
                      setState(() {
                        newAmaunt++;
                        if(timer.isActive) timer.cancel();
                        timer= Timer(Duration(seconds: 2), (){
                          update.changeValue(token, newAmaunt, widget.busketItem.basketId);
                        });
                      });
                      update.changeValueBasketLocal(true, widget.busketItem.basketId,token);
                    },
                    child: Container(
                      width: 32,
                      height: 32,
                      decoration: BoxDecoration(
                          color: Theme.of(context).primaryColor,
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(8), bottomLeft: Radius.circular(8))),
                      child: Center(
                          child: Icon(
                            Icons.add,
                            color: Colors.white,
                          )),
                    ),
                  ),
                  Container(
                    width: 48,
                    height: 32,
                    decoration: BoxDecoration(
                      color: Color(0xffA50808),
                    ),
                    child: Center(
                        child: Text(
                          newAmaunt.toString() ?? '',
                          style: TextStyle(color: Colors.white, fontSize: 20),
                        )),
                  ),
                  GestureDetector(
                    onTap: (){
                      if(newAmaunt != 1)
                        setState(() {
                          newAmaunt--;
                          if(timer.isActive) timer.cancel();
                          timer= Timer(Duration(seconds: 2), () {
                            update.changeValue(token, newAmaunt, widget.busketItem.basketId);
                          });
                        });
                      update.changeValueBasketLocal(false, widget.busketItem.basketId,token);
                    },
                    child: Container(
                      width: 32,
                      height: 32,
                      decoration: BoxDecoration(
                          color: Theme.of(context).primaryColor,
                          borderRadius: BorderRadius.only(
                              topRight: Radius.circular(8),
                              bottomRight: Radius.circular(8))),
                      child: Center(
                          child: Icon(
                            Icons.remove,
                            color: Colors.white,
                          )),
                    ),
                  ),
                ],
              )
            )
          ],
        ),
      ),
    );
  }
}
