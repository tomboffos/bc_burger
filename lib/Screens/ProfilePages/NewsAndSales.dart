import 'package:bcburger/Screens/ProfilePages/NewsDescription.dart';
import 'package:bcburger/StateManagement/provide.dart';
import 'package:bcburger/Widgets/OrangeCircle.dart';
import 'package:bcburger/Widgets/ProfileDataFields.dart';
import 'package:bcburger/models/News.dart';
import 'package:bcburger/utils/loginProvider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
class NewsAndSales extends StatefulWidget {
  static var id = 'newsandsales';
  @override
  _NewsAndSalesState createState() => _NewsAndSalesState();
}

class _NewsAndSalesState extends State<NewsAndSales> {
  @override
  void initState(){
    // TODO: implement initState
    Provider.of<LoginProvide>(context,listen: false).findNews();
  }
  @override
  Widget build(BuildContext context) {
    var h = MediaQuery.of(context).size.height;
    var w = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        title: Text('Новости и акции',style: TextStyle(
          color: Colors.white,
          fontSize: 20,
          fontWeight: FontWeight.w700,
        ),),
      ),
      body: StreamBuilder<News>(
          stream: Provider.of<LoginProvide>(context,listen: false).newsController.stream.asBroadcastStream(),
        builder: (context, snapshot) {
          return snapshot.data != null && snapshot.data.success?  SafeArea(
            child:ListView.builder(itemBuilder: (context,i){
              return Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child:NewsInfoContainer(heroId: snapshot.data.data[i].id.toString(),newsSingle: snapshot.data.data[i],),
              );
            },itemCount: snapshot.data.data.length,),
          ) :snapshot.data!= null && snapshot.data.success == false ? Center(child: Text(snapshot.data.message),)  : OrangeCircle();
        }
      ),
    );
  }
}


class NewsInfoContainer extends StatelessWidget {
  final heroId;
  final NewsSingle newsSingle;
  NewsInfoContainer({this.heroId,this.newsSingle});
  @override
  Widget build(BuildContext context) {
    var w =MediaQuery.of(context).size.width;
    DateTime dateCreate = newsSingle.createdAt != null ?  DateTime.parse(newsSingle?.createdAt)  : DateTime.now();
    var h =MediaQuery.of(context).size.height;
    return Padding(
      padding: EdgeInsets.only(top: 16),
      child: Container(
        width: w,
        decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 5,
              blurRadius: 7,
              offset: Offset(0, 3), // changes position of shadow
            ),
          ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(padding: EdgeInsets.only(left: 20,top: 16,bottom: 16,right: 16), child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(dateCreate.year.toString() + "-" + dateCreate.month.toString() + "-" + dateCreate.day.toString()),
                Text(dateCreate.hour.toString() + ":" + dateCreate.minute.toString()),
              ],
            ),),
            Hero(
              tag: heroId,
              child: Container(
                width: w,
                height: 170,
                decoration: BoxDecoration(
                  image: DecorationImage(image: NetworkImage(newsSingle.images),fit: BoxFit.fitWidth),
                ),
              ),
            ),
            Padding(padding: EdgeInsets.only(left: 24,top: 16,right: 24,bottom: 16), child:
            Container(
              width: w,
              child: Text(newsSingle.name,style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.bold
              ),textAlign: TextAlign.center,),
            ),),
            GestureDetector(
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context) => NewsDescription(heroId: heroId,newsSingle:newsSingle)));
              },
              child: Container(
                width: w,
                height: 50,
                decoration: BoxDecoration(
                  color: Theme.of(context).primaryColor,
                  borderRadius: BorderRadius.only(bottomLeft: Radius.circular(8),bottomRight: Radius.circular(8))
                ),
                child: Center(
                  child: Text(
                    'Смотреть',
                    style: TextStyle(
                      color: Colors.white,
                        fontSize: 16,
                        fontWeight: FontWeight.bold
                    ),
                  ),
                ),
              ),
            )

          ],
        ),
      ),
    );
  }
}

