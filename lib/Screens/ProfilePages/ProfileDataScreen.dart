import 'package:bcburger/Screens/bottomNavBar.dart';
import 'package:bcburger/Widgets/ProfileDataFields.dart';
import 'package:bcburger/Widgets/CustomTextField.dart';
import 'package:bcburger/models/user.dart';
import 'package:bcburger/utils/loginProvider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:bcburger/Widgets/ActionButton.dart';
class ProfileDataScreen extends StatefulWidget {
  static var id = 'profileDatascreen';

  @override
  _ProfileDataScreenState createState() => _ProfileDataScreenState();
}

class _ProfileDataScreenState extends State<ProfileDataScreen> {
  bool closed = true;
  @override
  Widget build(BuildContext context) {
    var loginProvide = Provider.of<LoginProvide>(context);
    var newData = Provider.of<LoginProvide>(context).newDataForUpdate;
    var h = MediaQuery.of(context).size.height;
    var w = MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: AppBar(
        leading: GestureDetector(child: Icon(Icons.arrow_back,color: Colors.black,),onTap: (){
          Navigator.pop(context);
        },),
        backgroundColor: Colors.white,
        title: Text('Профиль',style: TextStyle(
          color: Color(0xff1A1A1A),
          fontSize: 18,
          fontWeight: FontWeight.w700,
        ),),
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.only(top: 24),
          child: Container(
            height: h-42,
            width: w,
            child: SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  CustomTextField(headText: 'Ваше имя',onChanged: (val){
                    newData['name'] = val;
                  },hintText: loginProvide.user.name,),
                  SizedBox(height: 28,),
                  CustomTextField(headText: 'Телефон',onChanged: (val){
                    newData['tel'] = val;
                    },hintText: loginProvide.user.tel,),
                  SizedBox(height: 28,),
                  CustomTextField(headText: 'E-mail',onChanged: (val){
                    newData['email'] = val;
                    },hintText: loginProvide.user.email,),
                  SizedBox(height: 28,),
                  CustomTextField(headText: 'Пароль',obscure: true,onChanged: (val){
                    newData['password'] = val.toString();
                    },),
                  SizedBox(height: 28,),
                  CustomTextField(headText: 'Введите пароль повторно',obscure: true,onChanged: (val){
                    newData['password_confirmation'] = val.toString();
                    },),
                  SizedBox(height: 28,),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 32),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Район доставки',
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 12,
                              fontWeight: FontWeight.w600
                          ),
                        ),
                        SizedBox(height: 8,),
                        SingleChildScrollView(
                          child: Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(8),
                                border: Border.all(color: Colors.grey)
                            ),
                            child: ExpansionTile(
                              onExpansionChanged: (val){
                                setState(() {
                                  closed = !closed;
                                });
                              },
                              title: Text(newData['district'] != '' && newData['district'] != null ? newData['district'] : (loginProvide.user.district != null ? loginProvide.user.district: 'Республикии, 4'),style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.normal,
                                  fontSize: 16
                              ),),
                              trailing: Icon( closed ? Icons.arrow_drop_down : Icons.arrow_drop_up,color: Theme.of(context).primaryColor,size: 30,),
                              children: loginProvide.adressesArray.map((e) => Column(
                                children: [
                                  Divider(thickness: 1,),
                                  ListTile(
                                    onTap: (){
                                      loginProvide.chengeRaionInProfile(e);
                                    },
                                    title: Text(e),
                                  ),
                                ],
                              )).toList(),

                            ),
                          ),
                        ),
//                        Container(
//                          width: w,
//                          padding: const EdgeInsets.only(left: 10.0, right: 10.0),
//                          decoration: BoxDecoration(
//                              borderRadius: BorderRadius.circular(8.0),
//                              border: Border.all()),
//                          child: DropdownButtonHideUnderline(
//                            child: DropdownButton(
//                                iconEnabledColor: Theme.of(context).primaryColor,
//                                iconSize: 32,
//                                value: newData['district'] != '' && newData['district'] != null ? newData['district'] : loginProvide.raion,
//                                items: loginProvide.adressesArray.map((e) => DropdownMenuItem<String>(
//                                  value: e,
//                                  child: Text(e),
//                                )).toList(),
//                                onChanged: (value) {
//                                  loginProvide.chengeRaionInProfile(value);
//                                }),
//                          ),
//                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 28,),
                  CustomTextField(headText: 'Адресс доставки',onChanged: (val){
                    newData['address'] = val;
                    },hintText: loginProvide.user.address,),
                  SizedBox(height: 28,),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 32),
                    child: Row(children: [
                      Container(

                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Номер квартиры',
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 12,
                                  fontWeight: FontWeight.w600
                              ),
                            ),
                            SizedBox(height: 8,),
                            Container(

                              width: w/2 -40,
                              height: 50,
                              child: TextField(

                                cursorColor: Colors.black,
                                decoration: new InputDecoration(

                                  hintText: loginProvide.user.apartment,
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: Colors.black),
                                    borderRadius: new BorderRadius.circular(8.0),
                                  ),
                                  border: new OutlineInputBorder(
                                    borderRadius: new BorderRadius.circular(8.0),
                                    borderSide: new BorderSide(color:  Colors.black),
                                  ),
                                ),
                                keyboardType: TextInputType.number,
                                style: new TextStyle(

                                  color: Colors.black,

                                ),

                                onChanged: (val){
                                  newData['apartment'] = val.toString();
                                },

                              ),


                            ),
                          ],
                        ),
                      ),

                      SizedBox(width: 16,),
                      Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Подъезд',
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 12,
                                  fontWeight: FontWeight.w600
                              ),
                            ),
                            SizedBox(height: 8,),
                            Container(
                              width: w/2 -40,
                              height: 50,
                              child: TextField(

                                cursorColor: Colors.black,
                                decoration: new InputDecoration(

                                  hintText: loginProvide.user.entrance,
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: Colors.black),
                                    borderRadius: new BorderRadius.circular(8.0),
                                  ),
                                  border: new OutlineInputBorder(
                                    borderRadius: new BorderRadius.circular(8.0),
                                    borderSide: new BorderSide(color:  Colors.black),
                                  ),
                                ),
                                keyboardType: TextInputType.number,
                                style: new TextStyle(

                                  color: Colors.black,

                                ),
                                onChanged: (val){
                                  newData['entrance'] = val.toString();
                                },

                              ),


                            ),
                          ],
                        ),
                      ),
                    ],),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 32,right:32,top: 32,bottom: 64),
                    child: ActionButton(trigger:loginProvide.isLoading, text: 'Сохранить',enable:true,onTap: (){
                      print('qqq---------------------------');

                      print(loginProvide.user.district);
                      print('--------passss---------');
                      print(loginProvide.password);
                      print('--------telNumber---------');
                      print(loginProvide.user.tel);


                      var token = Provider.of<LoginProvide>(context,listen: false).user.token;
                      loginProvide.updateUser(context,token);
                    },),
                  ),
                  SizedBox(height: 120,),
                ],
              ),
            ),
          ),
        ),
      ),

    );

  }
}



