import 'dart:convert';

import 'package:bcburger/StateManagement/provide.dart';
import 'package:bcburger/Widgets/ActionButton.dart';
import 'package:bcburger/Widgets/CustomTextField.dart';
import 'package:bcburger/Widgets/OrangeCircle.dart';
import 'package:bcburger/models/AboutUs.dart';
import 'package:bcburger/utils/loginProvider.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class AboutUs extends StatefulWidget {
  static var id = 'aboutUs';

  @override
  _AboutUsState createState() => _AboutUsState();
}

class _AboutUsState extends State<AboutUs> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getOferta();
  }
  var loading = true;
  AboutUsModel abotUs;
  Future getOferta()async{
    try{
      final responce = await  http.get('https://burger.a-lux.dev/api/page/aboutus/aboutus');
      var jsonData = json.decode(responce.body);
      if(jsonData['success']){
        print(jsonData['data']);
        setState(() {
          abotUs = AboutUsModel.fromJson(jsonData);
          print(abotUs.data.content);
          loading = false;
        });
      }

    }catch(e){
      print(e);
    }
  }
  @override
  Widget build(BuildContext context) {
    var h = MediaQuery.of(context).size.height;
    var w = MediaQuery.of(context).size.width;
    var mainTextStyle = TextStyle(
      fontWeight: FontWeight.w600,
      fontSize: 16,
    );
    var mainTextStyleLine = TextStyle(
      fontWeight: FontWeight.w500,
      fontSize: 16,
      color: Colors.black
    );
    var secondaryTextStyle = TextStyle(
      color: Color(0xff0D0D0D),
      fontWeight: FontWeight.normal,
      fontSize: 16,
    );
    var provide = Provider.of<LoginProvide>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('О нас',style: TextStyle(
          color: Colors.white,
          fontSize: 18,
          fontWeight: FontWeight.w700,
        ),),
      ),
      body: loading ? OrangeCircle() :  SafeArea(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 24,),
            child: SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 24,),
                  abotUs.data.heading != null ?  Html(
                    data: abotUs.data.heading,
                  ) : SizedBox(),
                  SizedBox(height: 12,),
                  abotUs.data.heading != null ?  Html(
                    data: abotUs.data.subHeading,
                  ) : SizedBox(),
//                  SizedBox(height: 12,),
//                  Text('Доставка ${provide.open.split(':')[0]+ ':' + provide.open.split(':')[1]}-${provide.close.split(':')[0]+ ':' + provide.close.split(':')[1]}',style: secondaryTextStyle),
//                  SizedBox(height: 16,),
//                  Container(height: 1,color: Theme.of(context).primaryColor,),
                  SizedBox(height: 16,),
//                  SizedBox(height: 18,),
                  ...abotUs.data.addresses.map((adress) => Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(height: 1,color: Theme.of(context).primaryColor,),
                        SizedBox(height: 8,),
                        Text(adress.name,style: mainTextStyle),
                        SizedBox(height: 8,),
                        ...adress.items.map((e){
                        if(e.type == 'phone' || e.type == 'point'){
                          return GestureDetector(
                              onTap: ()async{
                                if(e.type == 'phone'){
                                  await UrlLauncher.launch("tel://${e.name}");
                                }
                              },
                              child: InfoIcon(text: e.name,icon: e.type == 'phone' ? Icons.call : Icons.place,));
                        }else{
                          if(e.type == 'email' || e.type == 'inst'){
                            return GestureDetector(
                                onTap: ()async{
                                  if(e.type == 'inst'){
                                    await UrlLauncher.launch('https://www.instagram.com/${e.name}/');
                                  }else{
                                    await UrlLauncher.launch("mailto:${e.name}");
                                  }
                                },
                                child:e.type == 'inst' ? Padding(
                                  padding: const EdgeInsets.only(bottom: 16),
                                  child: Row(
                                    children: [
                                      Image.asset('assets/instagram.png',width: 23,),
                                      SizedBox(width: 24,),
                                      Text(e.name,style: secondaryTextStyle),
                                    ],
                                  ),
                                ) : InfoIcon(text: e.name,icon: Icons.mail,));
                          }else if(e.type == 'title'){
                            return Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
//                                Container(height: 1,color: Theme.of(context).primaryColor,),
                                SizedBox(height: 8,),
                                Text(e.name,style: mainTextStyleLine),
                                SizedBox(height: 12,)
                              ],
                            );
                          }
                        }
                      }).toList(),]
                    ),
                  )),
//                  ...abotUs.data.addresses[0].items.map((e){
//                    if(e.type == 'phone' || e.type == 'point'){
//                      return GestureDetector(
//                          onTap: ()async{
//                            if(e.type == 'phone'){
//                            await UrlLauncher.launch("tel://${e.name.split(':')[1]}");
//                            }
//                          },
//                          child: InfoIcon(text: e.name,icon: e.type == 'phone' ? Icons.call : Icons.place,));
//                    }else{
//                      return SizedBox();
//                    }
//                  }).toList(),
//                  Container(height: 1,color: Theme.of(context).primaryColor,),
//                  SizedBox(height: 16,),
//                  Text('B&C Burger Street 11.00-22.00',style: secondaryTextStyle),
//                  SizedBox(height: 16,),
//                  ...abotUs.data.addresses[0].items.map((e){
//                    if(e.type == 'email' || e.type == 'inst'){
//                      return GestureDetector(
//                          onTap: ()async{
//                            if(e.type == 'inst'){
//                              await UrlLauncher.launch('https://www.instagram.com/${e.name}/');
//                            }else{
//                              await UrlLauncher.launch("mailto:${e.name}");
//                            }
//                          },
//                          child:e.type == 'inst' ? Padding(
//                            padding: const EdgeInsets.only(bottom: 16),
//                            child: Row(
//                              children: [
//                                Image.asset('assets/instagram.png',width: 23,),
//                                SizedBox(width: 24,),
//                                Text(e.name,style: secondaryTextStyle),
//                              ],
//                            ),
//                          ) : InfoIcon(text: e.name,icon: Icons.mail,));
//                    }else{
//                      return SizedBox();
//                    }
//                  }).toList(),
                ],
              ),
            ),
          )
      ),
    );
  }
}


class InfoIcon extends StatelessWidget {
  final text;
  final icon;
  InfoIcon({this.text,this.icon});
  @override
  Widget build(BuildContext context) {
    var secondaryTextStyle = TextStyle(
      color: Color(0xff0D0D0D),
      fontWeight: FontWeight.normal,
      fontSize: 16,
    );
    return Padding(
      padding: const EdgeInsets.only(bottom: 16),
      child: Row(
        children: [
          Icon(icon,color: Theme.of(context).primaryColor,),
          SizedBox(width: 24,),
          Text(text,style: secondaryTextStyle),
        ],
      ),
    );
  }
}

