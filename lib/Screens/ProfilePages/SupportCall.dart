import 'package:bcburger/Widgets/ActionButton.dart';
import 'package:bcburger/Widgets/CustomTextField.dart';
import 'package:bcburger/utils/loginProvider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
class SupportCall extends StatelessWidget {
  static var id = 'supportCall';
  @override
  Widget build(BuildContext context) {
    var loginProvide = Provider.of<LoginProvide>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Обратный звонок',style: TextStyle(
          color: Colors.white,
          fontSize: 18,
          fontWeight: FontWeight.w700,
        ),),
      ),
      body: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(height: 24,),
              CustomTextField(
               headText: 'Имя',
                onChanged:(val) {
                 loginProvide.callChange('name', val);
                }

             ),
              SizedBox(height: 24,),
              CustomTextField(
                headText: 'Телефон',
                  onChanged:(val) {
                    loginProvide.callChange('tel', val);
                  }
              ),
              SizedBox(height: 32,),
              ActionButton(text: 'Оставить запрос',enable: true,onTap: (){
                if(loginProvide.callMap['name'] != null &&loginProvide.callMap['name'] != "" &&  loginProvide.callMap['tel'] != null && loginProvide.callMap['tel'] != ''){
                  loginProvide.getCall(context);
                }
              },trigger: loginProvide.supportCallBool,),
            ],
          )
      ),
    );
  }
}



