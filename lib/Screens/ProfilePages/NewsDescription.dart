import 'package:bcburger/Widgets/ProfileDataFields.dart';
import 'package:bcburger/models/News.dart';
import 'package:flutter/material.dart';
class NewsDescription extends StatelessWidget {
  final heroId;
  NewsSingle newsSingle;
  NewsDescription({this.heroId,this.newsSingle});
  static var id = 'newsandsalesdesc';
  @override
  Widget build(BuildContext context) {
    var h = MediaQuery.of(context).size.height;
    var w = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        title: Text('Новости и акции',style: TextStyle(
          color: Colors.white,
          fontSize: 20,
          fontWeight: FontWeight.w700,
        ),),
      ),
      body: Padding(
        padding: EdgeInsets.only(top: 24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(padding: EdgeInsets.only(left: 20,top: 16,bottom: 16), child: Text(newsSingle.createdAt.toString()),),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: Hero(
                tag: heroId,
                child: Container(
                  width: w-32,
                  height: 170,
                  decoration: BoxDecoration(
                    image: DecorationImage(image: NetworkImage(newsSingle.images),fit: BoxFit.fitWidth),
                  ),
                ),
              ),
            ),
            Padding(padding: EdgeInsets.only(left: 24,top: 16,right: 24,bottom: 24), child: Text(newsSingle.name,style: TextStyle(
                fontSize: 16,
                color: Theme.of(context).primaryColor,
                fontWeight: FontWeight.bold
            ),),),
            Expanded(
              child: Padding(
                padding:  EdgeInsets.only(left: 24,top: 16,right: 24,bottom: 24),
                child: Container(
                  child: SingleChildScrollView(
                    scrollDirection: Axis.vertical,
                    child: Text(newsSingle.content,style: TextStyle(
                        fontSize: 16,
                        color: Colors.black,
                        fontWeight: FontWeight.normal
                    ),),
                  ),
                ),
              ),
            ),


          ],
        ),
      )
    );
  }
}



