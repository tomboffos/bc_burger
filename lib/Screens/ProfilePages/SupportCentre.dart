
//loginProvide.changeWhom('whom', value);


import 'package:bcburger/Screens/bottomNavBar.dart';
import 'package:bcburger/Widgets/ActionButton.dart';
import 'package:bcburger/Widgets/ProfileDataFields.dart';
import 'package:bcburger/utils/loginProvider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
class SupportCentre extends StatefulWidget {
  static var id = 'supportCentre';

  @override
  _SupportCentreState createState() => _SupportCentreState();
}

class _SupportCentreState extends State<SupportCentre> {
  bool closed = true;
  var current = 0;
  @override
  Widget build(BuildContext context) {
    var loginProvide = Provider.of<LoginProvide>(context);
    List<DropdownMenuItem<String>> items = [DropdownMenuItem(
      child: Text('Связаться с администратором'),
      value: 'Связаться с администратором' ,
    ),DropdownMenuItem(
      child: Text('Связаться с техническим специалистом'),
      value: 'Связаться с техническим специалистом' ,
    ),];
    List names = ['Связаться с администратором','Связаться с техническим специалистом'];
    String dropdownValue = items[0].value;
    var h = MediaQuery.of(context).size.height;
    var w = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        title: Text('Служба поддержки',style: TextStyle(
          color: Colors.white,
          fontSize: 18,
          fontWeight: FontWeight.w700,
        ),),
      ),
      body: SafeArea(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 32,vertical: 24),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'С кем хотите связаться?',
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 12,
                          fontWeight: FontWeight.w600
                      ),
                    ),
                    SizedBox(height: 8,),
                    SingleChildScrollView(
                      child: Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8),
                            border: Border.all(color: Colors.grey)
                        ),
                        child: ExpansionTile(
                          onExpansionChanged: (val){
                            setState(() {
                              closed = !closed;
                            });
                          },
                          title: Text(names[current],style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.normal,
                              fontSize: 16
                          ),),
                          trailing: Icon( closed ? Icons.arrow_drop_down : Icons.arrow_drop_up,color: Theme.of(context).primaryColor,size: 30,),
                          children: names.map((e) => Column(
                            children: [
                              Divider(thickness: 1,),
                              ListTile(
                                onTap: (){
                                  setState(() {
                                    current = names.indexOf(e);
                                  });
                                  loginProvide.changeWhom('whom', e);
                                },
                                title: Text(e),
                              ),
                            ],
                          )).toList(),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 32),
                child: Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Напишите сообщение',
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 12,
                            fontWeight: FontWeight.w600
                        ),
                      ),
                      SizedBox(height: 8,),
                      Container(
                        width: w -64,
                        height: 150,
                        child: TextField(
                          cursorColor: Colors.black,
                          decoration: new InputDecoration(
                            hintText: 'Сообщение',
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.black),
                              borderRadius: new BorderRadius.circular(8.0),
                            ),
                            border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(8.0),
                              borderSide: new BorderSide(color:  Colors.black),
                            ),
                          ),
                          maxLines: 8,
                          keyboardType: TextInputType.emailAddress,
                          style: new TextStyle(
                            color: Colors.black,
                          ),
                          onChanged: (val){
                            loginProvide.changeWhom('message', val);
                          },
                        ),
                      ),
                      SizedBox(height: 32,),
                      ActionButton(text: 'Отправить',enable: (loginProvide.supportMap['message']!= null && loginProvide.supportMap['message']!= ''),onTap: (){
                        if(loginProvide.supportMap['message']!= null && loginProvide.supportMap['message']!= ''){
                          loginProvide.getSupport(context);
                        }
                      },trigger: loginProvide.supportBool,),

                    ],
                  ),
                ),
              ),
            ],
          )
      ),
    );
  }
}




