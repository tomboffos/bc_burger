import 'package:bcburger/Widgets/LoadingCircle.dart';
import 'package:bcburger/Widgets/OrangeCircle.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/services.dart';
import 'package:html/dom.dart' as dom;
import 'package:webview_flutter/webview_flutter.dart';
class Politics extends StatefulWidget {
  static var id = 'policy';

  @override
  _PoliticsState createState() => _PoliticsState();
}

class _PoliticsState extends State<Politics> {

//  @override
//  void initState() {
//    // TODO: implement initState
//    super.initState();
//    getOferta();
//  }
//  var loading = true;
//  Future getOferta()async{
//    try{
//      final responce = await  http.get('https://burger.a-lux.dev/public/api/page/politics/politics');
//      setState(() {
//        htmdDoc  = responce.body;
//        loading = false;
//      });
//    }catch(e){
//      print(e);
//    }
//  }
  var htmdDoc = '';
  @override
  Widget build(BuildContext context) {
    var w = MediaQuery.of(context).size.width;
    var h = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        title: Text('Политика конфиденциальности',style: TextStyle(
          color: Colors.white,
          fontSize: 15,
          fontWeight: FontWeight.w700,
        ),),
      ),
      body: SafeArea(
          child:  Center(
            child:Container(
                width: w,
                height: h,
                child: WebView(
                  initialUrl: 'https://burger.a-lux.dev/public/api/page/politics/politics',
                )
            ),
          )
      ),
    );
  }
}
//FutureBuilder<String>(
//future: loadAsset(),
//builder: (context, snapshot) {
//return Padding(
//padding: const EdgeInsets.only(top: 32,left: 16,right: 16,bottom: 16),
//child: Text(snapshot?.data ?? '',style: TextStyle(
//fontSize: 14,
//fontWeight: FontWeight.w500
//),),
//);
//}
//),