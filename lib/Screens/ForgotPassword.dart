import 'package:bcburger/Screens/bottomNavBar.dart';
import 'package:bcburger/Screens/registration.dart';
import 'package:bcburger/Widgets/ActionButton.dart';
import 'package:bcburger/Widgets/CustomTextField.dart';
import 'package:bcburger/utils/loginProvider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
class ForgotPassword extends StatelessWidget {
  static var id = 'forgotPassword';
  @override
  Widget build(BuildContext context) {
    var h = MediaQuery.of(context).size.height;
    var w = MediaQuery.of(context).size.width;
    var loginProvide = Provider.of<LoginProvide>(context);
    var canTap = loginProvide.forgotLogin.length > 4 && loginProvide.forgotLogin.contains('@');
    return Scaffold(
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(top:16.0),
              child: Text('Восстановление пароля',style: TextStyle(color: Colors.black,fontSize: 20,fontWeight: FontWeight.bold),),
            ),
            Flexible(
              flex: 1,
              child: SizedBox(height: h,),
            ),
            CustomTextField(headText: 'E-mail',onChanged: (val){
              loginProvide.forgotLogin = val;
            },),
            SizedBox(height: 24,),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 32),
              child: ActionButton(text: 'Отправить письмо',enable:canTap,onTap: (){
                if(canTap){
                  loginProvide.sendEmail(context);
                }
              },trigger: loginProvide.forgotLoading,),
            ),
            Flexible(
              flex: 3,
              child: SizedBox(height: h,),
            ),
          ],
        ),
      ),
    );
  }
}

class RegisterButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var w = MediaQuery.of(context).size.width;
    var theme = Theme.of(context);
    return Container(
      width: w-32,
      padding:  EdgeInsets.symmetric(vertical: 16),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        border: Border.all(color: theme.primaryColor),
      ),
      child: Center(
        child: Text(
          'Регистрация',style: TextStyle(color: theme.primaryColor,fontWeight: FontWeight.bold,fontSize: 16),
        ),
      ),
    );
  }
}
