import 'package:bcburger/Screens/ForgotPassword.dart';
import 'package:bcburger/Screens/bottomNavBar.dart';
import 'package:bcburger/Screens/registration.dart';
import 'package:bcburger/Widgets/ActionButton.dart';
import 'package:bcburger/Widgets/CustomTextField.dart';
import 'package:bcburger/utils/loginProvider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
class SecondAuthorizationSceen extends StatelessWidget {
  static var id = '2auth';
  @override
  Widget build(BuildContext context) {
    var h = MediaQuery.of(context).size.height;
    var w = MediaQuery.of(context).size.width;
    var loginProvide = Provider.of<LoginProvide>(context);
    var canTap = loginProvide.email.length > 4 && loginProvide.email.contains('@') && loginProvide.password.length >= 5;
    return Scaffold(
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(top:16.0),
              child: Text('Авторизация',style: TextStyle(color: Colors.black,fontSize: 20,fontWeight: FontWeight.bold),),
            ),
            Flexible(
              flex: 1,
              child: SizedBox(height: h,),
            ),
            CustomTextField(headText: 'E-mail',onChanged: (val){
              loginProvide.email = val;
            },),
            SizedBox(height: 28,),
            CustomTextField(headText: 'Пароль',obscure: true,onChanged: (val){
              loginProvide.password = val;
            },),
            Padding(
              padding: const EdgeInsets.only(left: 32,right:32,top: 8,bottom: 32),
              child: Container(width: w-64,
              child: GestureDetector(
                onTap: (){
                  Navigator.pushNamed(context, ForgotPassword.id);
                },
                child: Text('Забыл пароль',style: TextStyle(
                  color: Colors.black,
                  fontSize: 13,
                  fontWeight: FontWeight.w600,
                ),textAlign: TextAlign.end,),
              ),),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 32),
              child: GestureDetector(
                  onTap: (){
                    Navigator.pushNamed(context, RegisterScreen.id);
                  },
                  child: Text('Регистрация',style: TextStyle(fontSize: 16,fontWeight: FontWeight.w600,decoration: TextDecoration.underline),)),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 32),
              child: ActionButton(text: 'Войти',enable:canTap,onTap: (){
                if(canTap){
                  loginProvide.startLoading();
                  loginProvide.login(context);
                }
              },trigger: loginProvide.isLoading,),
            ),
            Flexible(
              flex: 3,
              child: SizedBox(height: h,),
            ),
          ],
        ),
      ),
    );
  }
}

class RegisterButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var w = MediaQuery.of(context).size.width;
    var theme = Theme.of(context);
    return Container(
      width: w-32,
      padding:  EdgeInsets.symmetric(vertical: 16),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        border: Border.all(color: theme.primaryColor),
      ),
      child: Center(
        child: Text(
          'Регистрация',style: TextStyle(color: theme.primaryColor,fontWeight: FontWeight.bold,fontSize: 16),
        ),
      ),
    );
  }
}
