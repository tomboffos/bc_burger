import 'package:bcburger/Screens/korzinaScreen.dart';
import 'package:bcburger/Widgets/LikeButton.dart';
import 'package:bcburger/Widgets/LoadingCircle.dart';
import 'package:bcburger/Widgets/ProfileDataFields.dart';
import 'package:bcburger/Widgets/busketIcon.dart';
import 'package:bcburger/models/Products.dart';
import 'package:bcburger/utils/loginProvider.dart';
import 'package:bcburger/utils/productsProvider.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
class ProductPage extends StatelessWidget {
  final heroId;
  final int idItem;
  final image;
  final name;
  final content;
  final bool inFav;
  final gram;
  final price;
  ProductPage({this.heroId,this.inFav,this.image,this.content,this.price,this.idItem,this.name,this.gram,});
  static var id = 'productPage';
  @override
  Widget build(BuildContext context) {
    var h = MediaQuery.of(context).size.height;
    var mainTextStyle = TextStyle(
      fontWeight: FontWeight.w600,
      fontSize: 16,
    );
    var secondaryTextStyle = TextStyle(
      color: Color(0xff0D0D0D),
      fontWeight: FontWeight.normal,
      fontSize: 12,
    );
    var w = MediaQuery.of(context).size.width;
    var loginProvide = Provider.of<LoginProvide>(context).provideBusketItems?.data?.basket?.length ?? 0;
    return Scaffold(
        appBar: AppBar(
          actions: [
           BusketIcon(loginProvide: loginProvide,),
            SizedBox(
              width: 16,
            ),
          ],
        ),
        body: Center(
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 2,
                  blurRadius: 7,
                  offset: Offset(0, 3), // changes position of shadow
                ),
              ],
            ),
            width: w-32,
            height: h*0.8,
            child: Stack(
              children: [
                Column(
                  children: [
                    Hero(
                      tag: heroId,
                      child: Container(
                        width: w-32,
                        height: 270,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(topLeft: Radius.circular(8),topRight: Radius.circular(8)),
                          image: DecorationImage(
                              image: image != null ?  CachedNetworkImageProvider(
                               image
                              ) : AssetImage('assets/dishes.png'), fit: BoxFit.fitHeight),
                        ),
                      ),
                    ),
                    SizedBox(height: 8,),
                    Text(name ?? '',style: mainTextStyle,),
                    SizedBox(height: 8,),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16),
                      child: Container(
                        width: w,
                        height: 1,
                        color: Theme.of(context).primaryColor,
                      ),
                    ),
                    SizedBox(height: 8,),
                    Text(gram ?? '',style: TextStyle(
                        fontSize: 12,
                        color: Color(0xff5E5E5E)
                    ),),
                    SizedBox(height: 8,),
                    LimitedBox(
                      maxHeight: 80,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 12),
                        child: Text(content ?? '',style: secondaryTextStyle,textAlign: TextAlign.center,),
                      ),
                    ),
                    Flexible(
                      child: SizedBox(
                        height: h,
                      ),
                    ),
                    GestureDetector(
                      onTap: (){
                        var user = Provider.of<LoginProvide>(context,listen: false).user;
                        Provider.of<LoginProvide>(context,listen: false).addToBusket(idItem,user.token,context);
                      },
                      child: Container(
                        width: w-32,
                        height: 50,
                        decoration: BoxDecoration(
                            color: Theme.of(context).primaryColor,
                            borderRadius: BorderRadius.only(bottomRight: Radius.circular(8),bottomLeft: Radius.circular(8))
                        ),
                        child:Provider.of<LoginProvide>(context,).isAddingItem != idItem ? Center(child: Text(
                          'В корзину за $price',style: TextStyle(
                          color: Colors.white,
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                        ),
                        ),) : LoadingCircle(),
                      ),
                    ),


                  ],
                ),
               if((inFav ?? true) == true) Positioned(top: 16,right: 16,
                  child: LikeButton(id: idItem,),),

              ],
            ),
          ),
        ),
    );
  }
}



