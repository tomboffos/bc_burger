import 'dart:ui';
import 'package:bcburger/Screens/korzinaScreen.dart';
import 'package:bcburger/StateManagement/provide.dart';
import 'package:bcburger/Widgets/LoadingCircle.dart';
import 'package:bcburger/Widgets/OrangeCircle.dart';
import 'package:bcburger/Widgets/SearchWidget.dart';
import 'package:bcburger/Widgets/busketIcon.dart';
import 'package:bcburger/models/Categories.dart';
import 'package:bcburger/models/Products.dart';
import 'package:bcburger/utils/loginProvider.dart';
import 'dart:ui';
import 'package:bcburger/utils/productsProvider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';
import 'package:bcburger/Widgets/SecondaryContainer.dart';
class MenuItemPage extends StatefulWidget {
  @override
  _MenuItemPageState createState() => _MenuItemPageState();
}

class _MenuItemPageState extends State<MenuItemPage> {
  void initState() {
    // TODO: implement initState
    var token = Provider.of<LoginProvide>(context,listen: false).user.token;
    var currentItem = Provider.of<ProductProvide>(context,listen: false).selectedItem;
//    Provider.of<ProductProvide>(context,listen: false).fetchProductsinCat(currentItem+1);
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    var w = MediaQuery.of(context).size.width;
    var h = MediaQuery.of(context).size.height;
    var currentItem = Provider.of<ProductProvide>(context).selectedItem;
    var maps = Provider.of<ProductProvide>(context).mapOfDownCats;
    var categories = Provider.of<ProductProvide>(context).categoriesProvide;
    var loginProvide = Provider.of<LoginProvide>(context).provideBusketItems?.data?.basket?.length ?? 0;
    var snapshot = Provider.of<ProductProvide>(context).productsProvide;
    var load = Provider.of<ProductProvide>(context).isProductLoading;
    return Scaffold(
        appBar: AppBar(
          title: Text( categories.data.where((element) => element.id == currentItem).toList()[0].name,
            style: TextStyle(
              color: Colors.white,
              fontSize: 18,
              fontWeight: FontWeight.w700,
            ),
          ),
          actions: [
           BusketIcon(loginProvide: loginProvide,),
            SizedBox(
              width: 8,
            ),
            SearchButton(),
            SizedBox(
              width: 16,
            ),
          ],
        ),
      body:  snapshot != null ?  Stack(
        children: [
          if(categories != null)  Positioned(
            top: 24,
            child: Container(
              width: w,
              height: 50,
              child: ListView.builder(itemBuilder: (context,i){
                return ItemContainer(number: categories.data[i].id,categories: categories);
              },scrollDirection: Axis.horizontal,itemCount: categories.data.length,),
            ),
          ),
          Positioned(
            top: 106,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8,),
              child: Container(
                width: w-16,
                height: h-200,
                child: (maps.containsKey(currentItem.toString()) ?? false) ?  GridView.builder(gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisSpacing: 8,
                    mainAxisSpacing: 24,
                    childAspectRatio:  (w/2-20)/340,
                    crossAxisCount:  2 ),
                    shrinkWrap: true,
                    physics: AlwaysScrollableScrollPhysics(),
                    itemBuilder: (context, i){
                      return  SecondaryContainer(id: maps[currentItem.toString()].data[i].id,
                        image: maps[currentItem.toString()].data[i].image,
                        content: maps[currentItem.toString()].data[i].content,
                        name: maps[currentItem.toString()].data[i].name,
                        gram: maps[currentItem.toString()].data[i].edMassa,
                        price: maps[currentItem.toString()].data[i].price,
                      );
                    },itemCount: maps[currentItem.toString()].data.length ) : OrangeCircle(),
              ),
            ),
          ),
        ],
      ) :  ShimmerMenu(),
    );





  }
}
class ShimmerMenu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var w = MediaQuery.of(context).size.width;
    var h = MediaQuery.of(context).size.height;
    return Padding(
      padding: const EdgeInsets.only(top: 24,left: 16,right: 16),
      child: Container(
        width: w-32,
        height: h,
        child: Shimmer.fromColors(
            baseColor: Colors.grey[300],
            highlightColor: Colors.grey[100],
            child: SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    width: w,
                    height: 50,
                    child: ListView.builder(itemBuilder: (context,i){
                      return Padding(
                        padding: const EdgeInsets.only(right: 16),
                        child: Container(
                          width: 130,
                          height: 50,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(36),
                            color: Colors.white,
                          ),
                        ),
                      );
                    },scrollDirection: Axis.horizontal,itemCount: 7,),
                  ),
                  SizedBox(height: 32,),
                  Row(children: [
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        color: Colors.white,
                      ),
                      width: w/2-20,
                      height: 340,
                    ),
                    SizedBox(width: 16,)

                    ,                  Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        color: Colors.white,
                      ),
                      width: w/2-20,
                      height: 340,
                    ),

                  ],),
                  SizedBox(height: 16,),
                  Row(children: [
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        color: Colors.white,
                      ),
                      width: w/2-20,
                      height: 340,
                    ),
                    SizedBox(width: 16,)

                    ,                  Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        color: Colors.white,
                      ),
                      width: w/2-20,
                      height: 340,
                    ),

                  ],)

                ],
              ),
            )
        ),
      ),
    );
  }
}

class ItemContainer extends StatelessWidget {
  final Categories categories;
  final number;
  ItemContainer({this.number,this.categories});
  @override
  Widget build(BuildContext context) {
    var provide = Provider.of<ProductProvide>(context);
    return Padding(
      padding: const EdgeInsets.only(right: 8,left: 8),
      child: GestureDetector(
        onTap: (){
          if(number != provide.selectedItem){
            Provider.of<ProductProvide>(context,listen: false).changeSelectedItem(number);
            Provider.of<ProductProvide>(context,listen:false).fetchProductsinCat(number);
          }
        },
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(36),
            border: Border.all(color:Theme.of(context).primaryColor),
            color: provide.selectedItem == number ? Theme.of(context).primaryColor : Colors.white,
          ),
          padding: EdgeInsets.symmetric(horizontal: 16,vertical: 5),
          child: Center(child: Text(
            categories.data.where((element) => element.id == number).toList()[0].name,
            style: TextStyle(
              color:  provide.selectedItem != number ? Theme.of(context).primaryColor : Colors.white,
              fontSize: 16,
            ),
          ),),
        ),
      ),
    );

  }
}

//return Scaffold(
//appBar: AppBar(
//title: Text( categories.data[currentItem].name,
//style: TextStyle(
//color: Colors.white,
//fontSize: 18,
//fontWeight: FontWeight.w700,
//),
//),
//actions: [
//GestureDetector(
//onTap: (){
//Navigator.pushNamed(context, KorzinaScreen.id);
//},
//child: Icon(
//Icons.shopping_cart,
//color: Colors.white,
//size: 30,
//),
//),
//SizedBox(
//width: 8,
//),
//Icon(
//Icons.search,
//color: Colors.white,
//size: 30,
//),
//SizedBox(
//width: 16,
//),
//],
//),
//body: FutureProvider<Products>(
//create: (context)=> productsFuture,
//child: Consumer<Products>(
//builder: (context, snapshot,child) {
//return snapshot != null ? Stack(
//children: [
//if(categories != null)  Positioned(
//top: 24,
//child: Container(
//width: w,
//height: 50,
//child: ListView.builder(itemBuilder: (context,i){
//return ItemContainer(number: i,categories: categories);
//},scrollDirection: Axis.horizontal,itemCount: categories.data.length,),
//),
//),
//if(snapshot.data.length  != 0 && snapshot.data.length != null) Positioned(
//top: 106,
//child: Padding(
//padding: const EdgeInsets.symmetric(horizontal: 8),
//child: Container(
//width: w-16,
//height: h-78,
//child: GridView.builder(gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
//crossAxisSpacing: 8,
//mainAxisSpacing: 24,
//childAspectRatio:(w/2-20)/340,
//crossAxisCount:  2 ), itemBuilder: (context, i){
//return SecondaryContainer(id: 'das' + snapshot.data[i].id.toString()+categories.data[currentItem].name,
//image: snapshot.data[i].image,
//content: snapshot.data[i].content,
//name: snapshot.data[i].name,
//gram: snapshot.data[i].edMassa,
//price: snapshot.data[i].price,
//);
//},itemCount: snapshot.data.length),
//),
//),
//)
//],
//) : LoadingCircle();
//}
//)
//),
//);