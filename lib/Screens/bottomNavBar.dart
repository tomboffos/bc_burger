import 'package:bcburger/Screens/BottomPages/Favoutites.dart';
import 'package:bcburger/Screens/BottomPages/HomePage.dart';
import 'package:bcburger/Screens/BottomPages/MyOrders.dart';
import 'package:bcburger/Screens/BottomPages/ProfileScreen.dart';
import 'package:flutter/material.dart';



class BottomNavBar extends StatefulWidget {
  BottomNavBar({Key key}) : super(key: key);
  static var id = 'bottomnavbar';
  @override
  _BottomNavBarState createState() => _BottomNavBarState();
}

class _BottomNavBarState extends State<BottomNavBar> {
  int _selectedIndex = 0;
  static const TextStyle optionStyle =
  TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  static  List<Widget> _widgetOptions = <Widget>[
    HomePage(),
    FavouriteScreen(),
    MyOrders(),
    ProfileScreen(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    const textStyle =TextStyle(
        fontSize: 10
    );
    return Scaffold(
      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text('Главная',style: textStyle),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.favorite),
            title: Text('Избранное',style: textStyle),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.fastfood),
            title: Text('Ваши заказы',style: textStyle),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            title: Text('Профиль',style: textStyle),
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Theme.of(context).primaryColor,
        unselectedItemColor: Color(0xffDADADA),
        backgroundColor: Colors.blue,
        onTap: _onItemTapped,
      ),
    );
  }
}