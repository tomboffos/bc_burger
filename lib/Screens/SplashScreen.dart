import 'package:bcburger/Screens/SecondAuthorization.dart';
import 'package:bcburger/Screens/authorization.dart';
import 'package:bcburger/Screens/bottomNavBar.dart';
import 'package:bcburger/models/user.dart';
import 'package:bcburger/utils/loginProvider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
class SplashScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    User user = Provider.of<LoginProvide>(context).user;
    return (user?.token ?? '') != '' ?  BottomNavBar() : AuthorizationSceen();
  }
}
