import 'package:bcburger/StateManagement/provide.dart';
import 'package:bcburger/Widgets/CustomTextField.dart';
import 'package:bcburger/Widgets/LoadingCircle.dart';
import 'package:bcburger/Widgets/OrangeCircle.dart';
import 'package:bcburger/utils/loginProvider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';
class PerceedScreen extends StatefulWidget {
  static var id = 'perceedScreen';

  @override
  _PerceedScreenState createState() => _PerceedScreenState();
}

class _PerceedScreenState extends State<PerceedScreen>
    with TickerProviderStateMixin {
  List<Widget> tabs;
  List<Widget> pages;
  TabController _controllerTab;
  TextEditingController timeCtl;
  TextEditingController loginField;
  @override
  void initState() {
    super.initState();
    var token = Provider.of<LoginProvide>(context, listen: false).user.token;
//    Provider.of<LoginProvide>(context, listen: false).currentBusket(token,cart: true);
    timeCtl = TextEditingController(text: DateTime.now().hour.toString()+':'+DateTime.now().minute.toString());
    _controllerTab = TabController(length: 2, vsync: this);
    Provider.of<LoginProvide>(context, listen: false).changeNotDeliverMap('payment', 'Картой курьеру');
    Provider.of<LoginProvide>(context, listen: false).changeDeliverMap('payment', 'Картой курьеру');
    Provider.of<LoginProvide>(context, listen: false).changeNotDeliverMap('delivery_time', 'Как можно скорее');
    Provider.of<LoginProvide>(context, listen: false).changeDeliverMap('delivery_time', 'Как можно скорее');

  }

  @override
  Widget build(BuildContext context) {
    var h = MediaQuery.of(context).size.height;
    var w = MediaQuery.of(context).size.width;
    var provide = Provider.of<Provide>(context);
    var loginProvide = Provider.of<LoginProvide>(context);
    var snapshot = loginProvide.provideBusketItems;
    tabs = [
      Container(
          width: w / 2.4,
          child: Tab(
            text: 'Доставка',
          )),
      Container(width: w / 2.4, child: Tab(text: 'Самовывоз')),
    ];

    return snapshot != null ? Scaffold(
      appBar: AppBar(
        title: Text(
          'Оформление заказа',
          style: TextStyle(
            color: Colors.white,
            fontSize: 20,
            fontWeight: FontWeight.w700,
          ),
        ),
        bottom: TabBar(
            indicatorColor: Colors.white,
            isScrollable: true,
            controller: _controllerTab,
            tabs: tabs),
      ),
      body: loginProvide.provideBusketItems.data != null ?  TabBarView(controller: _controllerTab, children: [
            Ship(w: w, h: h, provide: provide,price: snapshot.data.allPrice,),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Container(
                width: w - 32,
                height: h,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(8), topRight: Radius.circular(8)),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 2,
                      blurRadius: 7,
                      offset: Offset(0, 3), // changes position of shadow
                    ),
                  ],
                ),
                child: SingleChildScrollView(
                  scrollDirection: Axis.vertical,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 16,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 32),
                        child: Text(
                          'Наши точки доставки',
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 12,
                              fontWeight: FontWeight.w600),
                        ),
                      ),
                      SizedBox(
                        height: 24,
                      ),
                      ChooseOption(text: 'Ул. Мухита 78 Б',isWithMe: provide.isWithMe,myNumber: 0,onTap: (){
                        provide.changeIsWithMe(0);
                        loginProvide.changeNotDeliverMap('address', 'Ул. Мухита 78 Б');
                      },),
                      SizedBox(
                        height: 24,
                      ),
                      ChooseOption(text: 'мкр. Строитель 26/3',isWithMe: provide.isWithMe,myNumber: 1,onTap: (){
                        provide.changeIsWithMe(1);
                        loginProvide.changeNotDeliverMap('address', 'мкр. Строитель 26/3');
                      },),
                      SizedBox(
                        height: 24,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 32),
                        child: Text(
                          'Уточнить дату и время',
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 12,
                              fontWeight: FontWeight.w600),
                        ),
                      ),
                      SizedBox(
                        height: 24,
                      ),
                      ChooseOption(text: 'Как можно скорее',isWithMe: provide.shipPoints,myNumber: 0,onTap: (){
                        provide.changeShipPoint(0);
                        loginProvide.changeNotDeliverMap('delivery_time', 'Как можно скорее');
                        loginProvide.changeNotDeliverMap('delivery', 'Самовывоз');
                      },),
                      SizedBox(
                        height: 24,
                      ),
                      Row(
                        children: [
                          ChooseOption(text: 'Забрать в',isWithMe: provide.shipPoints,myNumber: 1,onTap: (){
                            provide.changeShipPoint(1);
                            loginProvide.changeNotDeliverMap('delivery_time', timeCtl.text);

                          },),
                          SizedBox(width: 16,),
                          Container(
                            child: Container(
                              width: 72,
                              height: 32,
                              child: TextFormField(
                                decoration: new InputDecoration(
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: Colors.black),
                                    borderRadius: new BorderRadius.circular(8.0),
                                  ),
                                  border: new OutlineInputBorder(
                                    borderRadius: new BorderRadius.circular(8.0),
                                    borderSide: new BorderSide(color:  Colors.black),
                                  ),
                                ),
                                controller: timeCtl,  // add this line.
                                onTap: () async {
                                  TimeOfDay time = TimeOfDay.now();
                                  FocusScope.of(context).requestFocus(new FocusNode());
                                  TimeOfDay picked =
                                  await showTimePicker(context: context, initialTime: time,helpText: 'Выберите время',cancelText: 'Отменить',confirmText: 'Готово',);
                                  if (picked != null && picked != time) {
                                    setState(() {
                                      time = picked;
                                      timeCtl.text = picked.format(context);
                                    });
                                    provide.changeShipPoint(1);
                                    loginProvide.changeNotDeliverMap('delivery_time', timeCtl.text);
                                  }
                                },
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return 'Заполните время';
                                  }
                                  return null;
                                },
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 32,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 32),
                        child: Text(
                          'Способ оплаты',
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 12,
                              fontWeight: FontWeight.w600),
                        ),
                      ),
                      SizedBox(
                        height: 24,
                      ),
                      Row(
                        children: [
                          ChooseOption(text: 'Картой курьеру',isWithMe: provide.cashType,myNumber: 0,onTap: (){
                            provide.changeCashType(0);
                            loginProvide.changeNotDeliverMap('payment', 'Картой курьеру');
                          },),
                          ChooseOption(text: 'Наличными',isWithMe: provide.cashType,myNumber: 1,onTap: (){
                            provide.changeCashType(1);
                            loginProvide.changeNotDeliverMap('payment', 'Наличными');
                          },),
                        ],
                      ),
                      SizedBox(
                        height: 24,
                      ),
                      CustomTextField(
                        headText: 'Нужна сдача с',
                        onChanged: (val){

                          loginProvide.changeNotDeliverMap('surrender', val);
                        },
                      ),
                      SizedBox(
                        height: 24,
                      ),
                      Container(
                        width: w,
                        height: 150,
                        decoration: BoxDecoration(
                            border: Border.all(width: 1,color: Theme.of(context).primaryColor),
                            borderRadius: BorderRadius.only(topRight: Radius.circular(10),topLeft: Radius.circular(10))
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(left: 32,top: 16),
                              child: Text(
                                'Итого:',
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                            SizedBox(
                              height: 8,
                            ),
                            SizedBox(
                              height: 8,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 32,top: 16,right: 32),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    'Сумма заказа:',
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 12,
                                        fontWeight: FontWeight.w500),
                                  ),
                                  Text(
                                    '${snapshot.data.allPrice} тг',
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 12,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 8,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 32,top: 16,right: 32),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    'Итого к оплате:',
                                    style: TextStyle(
                                        color: Theme.of(context).primaryColor,
                                        fontSize: 12,
                                        fontWeight: FontWeight.w500),
                                  ),
                                  Text(
                                    snapshot.data.allPrice.toString() + ' тг',
                                    style: TextStyle(
                                        color:  Theme.of(context).primaryColor,
                                        fontSize: 12,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      GestureDetector(
                        onTap: (){

                          if(provide.shipPoints == 1){
                            DateTime openTime = DateFormat("hh:mm:ss").parse(loginProvide.open);
                            DateTime closeTime = DateFormat("hh:mm:ss").parse(loginProvide.close);
                            DateTime hisTime = DateFormat("hh:mm").parse(loginProvide.notDeliverMap['delivery_time']);
                           if(closeTime.compareTo(hisTime) == 1 && openTime.compareTo(hisTime) == -1){
                               loginProvide.order(loginProvide.user.token,context,false);

                           }else {
                             loginProvide.showLoseConnection(
                                 context, 'Доставка недоступна',
                                 'Работаем с ${loginProvide
                                     .open} до ${loginProvide.close}', true);
                           }
                          }else{
                            loginProvide.order(loginProvide.user.token,context,false);
                          }
                        },
                        child: Container(
                          height: 50,
                          decoration: BoxDecoration(
                            color: Theme.of(context).primaryColor,
                          ),
                          child: loginProvide.isOrdering ? LoadingCircle() : Center(
                            child:  Text('Заказать за ${snapshot.data.allPrice} тг',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold
                              ),),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ]) : OrangeCircle(),

    ) : OrangeCircle();
  }
}

class Ship extends StatefulWidget {
  const Ship({
    Key key,
    @required this.w,
    @required this.h,
    @required this.provide,
    this.price,
  }) : super(key: key);
  final price;
  final double w;
  final double h;
  final Provide provide;

  @override
  _ShipState createState() => _ShipState();
}

class _ShipState extends State<Ship> {
  var snapshot;
  TextEditingController addressField = TextEditingController();
  TextEditingController appartmentField = TextEditingController();
  TextEditingController entranceField = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {

      addressField.text = Provider
          .of<LoginProvide>(context, listen: false)
          .user
          .address;

      Provider.of<LoginProvide>(context,listen:false).changeDeliverMap('address', addressField.text);

      //-----------------------------
      appartmentField.text = Provider
          .of<LoginProvide>(context, listen: false)
          .user.apartment;
      Provider.of<LoginProvide>(context,listen:false).changeDeliverMap('apartment', appartmentField.text);

      //------------------------------


      entranceField.text = Provider
          .of<LoginProvide>(context, listen: false)
          .user
          .entrance;

      Provider.of<LoginProvide>(context,listen:false).changeDeliverMap('entrance', entranceField.text);

    });
  }
  @override
  var closed = true;
  TextEditingController timeCtl = TextEditingController(text: DateTime.now().hour.toString()+':'+DateTime.now().minute.toString());
  Widget build(BuildContext context) {
    List adresses = Provider.of<LoginProvide>(context).adressesArray;
    var deliverMap = Provider.of<LoginProvide>(context).deliverMap;
    var loginProvide = Provider.of<LoginProvide>(context);



    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Container(
        width: widget.w - 32,
        height: widget.h,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(8), topRight: Radius.circular(8)),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 2,
              blurRadius: 7,
              offset: Offset(0, 3), // changes position of shadow
            ),
          ],
        ),
        child: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 16,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 32),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Район доставки',
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 12,
                          fontWeight: FontWeight.w600),
                    ),
                    SizedBox(
                      height: 8,
                    ),
//                    Container(
//                      width: widget.w,
//                      padding:
//                          const EdgeInsets.only(left: 10.0, right: 10.0),
//                      decoration: BoxDecoration(
//                          borderRadius: BorderRadius.circular(8.0),
//                          border: Border.all()),
//                      child: DropdownButtonHideUnderline(
//                        child: DropdownButton(
//                            iconEnabledColor:
//                                Theme.of(context).primaryColor,
//                            iconSize: 32,
//                            value:  deliverMap['district'] != '' && deliverMap['district'] != null ? deliverMap['district'] : loginProvide.raion,
//                            items: adresses.map((e) => DropdownMenuItem<String>(
//                              value: e,
//                              child: Text(e),
//                            )).toList(),
//                            onChanged: (value) {
//                              loginProvide.changeDeliverMap('district', value);
//                              loginProvide.changeOrderPrice(adresses.indexOf(value));
//                            }),
//                      ),
//                    ),
                  SingleChildScrollView(
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      border: Border.all(color: Colors.grey)
                    ),
                    child: ExpansionTile(
                      onExpansionChanged: (val){
                        setState(() {
                          closed = !closed;
                        });
                      },
                      title: Text(deliverMap['district'] != '' && deliverMap['district'] != null ? deliverMap['district'] : loginProvide.user.district ?? '',style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.normal,
                          fontSize: 16
                      ),),
                      trailing: Icon( closed ? Icons.arrow_drop_down : Icons.arrow_drop_up,color: Theme.of(context).primaryColor,size: 30,),
                      children: adresses.map((e) => Column(
                        children: [
                          Divider(thickness: 1,),
                          ListTile(
                            onTap: (){
                              loginProvide.changeDeliverMap('district', e);
                              loginProvide.changeOrderPrice(adresses.indexOf(e));
                            },
                            title: Text(e),
                          ),
                        ],
                      )).toList(),
                    ),
                  ),
                ),
                  ],
                ),
              ),
              SizedBox(
                height: 24,
              ),
              CustomTextField(
                textCtr: addressField,


                onChanged: (val){

                    loginProvide.changeDeliverMap('address', val);
                  },

                headText: 'Адресс доставки',
              ),
              SizedBox(
                height: 16,
              ),
              Padding(

                padding: const EdgeInsets.symmetric(horizontal: 0.1),

                child: Row(

                  //mainAxisAlignment: MainAxisAlignment.spaceBetween,

                  children: [
                    Container(


                      child: Column(



                        crossAxisAlignment: CrossAxisAlignment.start,
                        
                        children: [


                          // Text(
                          //   'Номер квартиры',
                          //   style: TextStyle(
                          //
                          //       color: Colors.black,
                          //       fontSize: 12,
                          //       fontWeight: FontWeight.w600),
                          // ),
                          // SizedBox(
                          //   height: 8,
                          // ),
                          Container(


                            width: widget.w / 2 - 22,
                            //margin: const EdgeInsets.only(right: 10),

                            child:
                              CustomTextField(

                                textCtr: appartmentField,


                                onChanged: (val){

                                  loginProvide.changeDeliverMap('apartment', val);
                                },

                                headText: 'Номер квартиры',
                              ),
                            //TextField(
                            //
                            //
                            //   cursorColor: Colors.black,
                            //   decoration: new InputDecoration(
                            //
                            //     hintText: loginProvide.user.apartment,
                            //     focusedBorder: OutlineInputBorder(
                            //       borderSide:
                            //           BorderSide(color: Colors.black),
                            //       borderRadius:
                            //           new BorderRadius.circular(8.0),
                            //     ),
                            //     border: new OutlineInputBorder(
                            //       borderRadius:
                            //           new BorderRadius.circular(8.0),
                            //       borderSide:
                            //           new BorderSide(color: Colors.black),
                            //     ),
                            //   ),
                            //   keyboardType: TextInputType.number,
                            //   style: new TextStyle(
                            //     color: Colors.black,
                            //   ),
                            //
                            //   onChanged: (val) {
                            //
                            //     loginProvide.changeDeliverMap('apartment', val);
                            //   },
                            // ),



                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                     // width: 16,
                    ),
                    Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          // Text(
                          //   'Подъезд',
                          //   style: TextStyle(
                          //       color: Colors.black,
                          //       fontSize: 12,
                          //       fontWeight: FontWeight.w600),
                          // ),
                          SizedBox(
                            height: 8,
                          ),
                          Container(

                            //padding: new EdgeInsets.only(left: 5 ),
                            width: widget.w / 2 - 27,

                             child:
                             CustomTextField(


                               textCtr: entranceField,


                               onChanged: (val){

                                 loginProvide.changeDeliverMap('entrance', val);
                               },

                               headText: 'Подъезд',
                             ),
                             //TextField(
                            //
                            //   cursorColor: Colors.black,
                            //   decoration: new InputDecoration(
                            //
                            //     hintText: loginProvide.user.entrance,
                            //
                            //     focusedBorder: OutlineInputBorder(
                            //       borderSide:
                            //           BorderSide(color: Colors.black),
                            //       borderRadius:
                            //           new BorderRadius.circular(8.0),
                            //     ),
                            //     border: new OutlineInputBorder(
                            //       borderRadius:
                            //           new BorderRadius.circular(8.0),
                            //       borderSide:
                            //           new BorderSide(color: Colors.black),
                            //     ),
                            //   ),
                            //   keyboardType: TextInputType.number,
                            //   style: new TextStyle(
                            //     color: Colors.black,
                            //   ),
                            //   onChanged: (val) {
                            //     loginProvide.changeDeliverMap('entrance', val);
                            //   },
                            // ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 24,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 32),
                child: Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                       'Комментарий курьеру',
                        style: TextStyle(

                            color: Colors.black,
                            fontSize: 12,
                            fontWeight: FontWeight.w600
                        ),
                      ),
                      SizedBox(height: 8,),
                      Container(
                        width:widget.w-64 ,
                        height: 150,
                        child: TextField(
                          maxLines: 5,
                          cursorColor: Colors.black,
                          decoration: new InputDecoration(
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.black),
                              borderRadius: new BorderRadius.circular(8.0),
                            ),
                            border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(8.0),
                              borderSide: new BorderSide(color:  Colors.black),
                            ),
                          ),
                          keyboardType:TextInputType.multiline,
                          style: new TextStyle(
                            color: Colors.black,
                          ),
                          onChanged: (val){
                            loginProvide.changeDeliverMap('comment', val);
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ),
//              CustomTextField(
//                onChanged: (val){
//                  loginProvide.changeDeliverMap('comment', val);
//                },
//                headText: 'Комментарий курьеру',
//                keyBoard: TextInputType.multiline,
//              ),
              SizedBox(
                height: 24,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 32),
                child: Text(
                  'Уточнить дату и время',
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 12,
                      fontWeight: FontWeight.w600),
                ),
              ),
              SizedBox(
                height: 24,
              ),
              ChooseOption(text: 'Как можно скорее',isWithMe: widget.provide.isWithMe,myNumber: 0,onTap: (){
                widget.provide.changeIsWithMe(0);
                loginProvide.changeDeliverMap('delivery', 'Доставить ко времени');
              },),
              SizedBox(
                height: 24,
              ),
              Row(
                children: [
                  ChooseOption(text: 'Доставить ко времени',isWithMe: widget.provide.isWithMe,myNumber: 1,onTap: (){
                    widget.provide.changeIsWithMe(1);
                    loginProvide.changeDeliverMap('delivery', 'Доставить ко времени');
                  },),
                  SizedBox(
                    width: 8,
                  ),
                  Container(
                    child: Container(
                      width: 72,
                      height: 32,
                      child: TextFormField(
                        decoration: new InputDecoration(
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.black),
                            borderRadius: new BorderRadius.circular(8.0),
                          ),
                          border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(8.0),
                            borderSide: new BorderSide(color:  Colors.black),
                          ),
                        ),
                        controller: timeCtl,  // add this line.
                        onTap: () async {

                          TimeOfDay time = TimeOfDay.now();
                          FocusScope.of(context).requestFocus(new FocusNode());
                          TimeOfDay picked =
                          await showTimePicker(context: context, initialTime: time,helpText: 'Выберите время',cancelText: 'Отменить',confirmText: 'Готово',);
                          if (picked != null && picked != time) {
                            timeCtl.text = picked.toString();  // add this line.
                            setState(() {
                              time = picked;
                              timeCtl.text = picked.format(context);
                            });
                            widget.provide.changeIsWithMe(1);
                            loginProvide.changeDeliverMap('delivery_time', timeCtl.text);
                          }
                        },
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Зполните время';
                          }
                          return null;
                        },
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 32,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 32),
                child: Text(
                  'Способ оплаты',
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 12,
                      fontWeight: FontWeight.w600),
                ),
              ),
              SizedBox(
                height: 24,
              ),
              Row(
                children: [
                  ChooseOption(text: 'Картой курьеру',isWithMe: widget.provide.cashType,myNumber: 0,onTap: (){
                    widget.provide.changeCashType(0);
                    loginProvide.changeDeliverMap('payment', 'Картой курьеру');
                  },),
                  ChooseOption(text: 'Наличными',isWithMe: widget.provide.cashType,myNumber: 1,onTap: (){
                    widget.provide.changeCashType(1);
                    loginProvide.changeDeliverMap('payment', 'Наличными');

                  },),
                ],
              ),
              SizedBox(
                height: 24,
              ),

              //----------------------------------------------------------------------------------------------
              CustomTextField(
                headText: 'Нужна сдача с',
                onChanged: (val){
                  print(val);
                  loginProvide.changeDeliverMap('surrender', val);
                },
              ),
              SizedBox(
                height: 24,
              ),

              Container(
                width: widget.w,
                height: 200,
                decoration: BoxDecoration(
                  border: Border.all(width: 1,color: Theme.of(context).primaryColor),
                  borderRadius: BorderRadius.only(topRight: Radius.circular(10),topLeft: Radius.circular(10))
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 32,top: 16),
                      child: Text(
                        'Итого:',
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 14,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    SizedBox(
                      height: 8,
                    ),



                    Padding(
                      padding: const EdgeInsets.only(left: 32,top: 16,right: 32),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Доставка:',
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 12,
                                fontWeight: FontWeight.w500),
                          ),
                          Text(
                            loginProvide.currentOrderPrice.toString() + ' тг',
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 12,
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 32,top: 16,right: 32),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Сумма заказа:',
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 12,
                                fontWeight: FontWeight.w500),
                          ),
                          Text(
                            this.widget.price.toString() + ' тг',
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 12,
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 32,top: 16,right: 32),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Итого к оплате:',
                            style: TextStyle(
                                color: Theme.of(context).primaryColor,
                                fontSize: 12,
                                fontWeight: FontWeight.w500),
                          ),
                          Text(
                            (loginProvide.currentOrderPrice + this.widget.price).toString() + ' тг',
                            style: TextStyle(
                                color:  Theme.of(context).primaryColor,
                                fontSize: 12,
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              GestureDetector(
                onTap: (){
                  print('------------Info------------------');
                  print(loginProvide.user.name);
                  print(loginProvide.user.email);
                  print(loginProvide.user.tel);
                  print(loginProvide.currentOrderPrice);
                  if(widget.provide.isWithMe == 1){
                    DateTime openTime = DateFormat("hh:mm:ss").parse(loginProvide.open);
                    DateTime closeTime = DateFormat("hh:mm:ss").parse(loginProvide.close);
                    DateTime hisTime = DateFormat("hh:mm").parse(loginProvide.deliverMap['delivery_time']);
                    if(closeTime.compareTo(hisTime) == 1  && openTime.compareTo(hisTime) == -1){
                        if((loginProvide.currentOrderPrice + widget.price) >= loginProvide.curentMinPrice){
                          Provider.of<LoginProvide>(context, listen: false)
                              .order(loginProvide.user.token, context,true);
                        }else{
                          loginProvide.showLoseConnection(context, 'Внимание!', 'минимальная цена для доставки: ${loginProvide.curentMinPrice} тг', true);
                        }
                    }else {
                      loginProvide.showLoseConnection(
                          context, 'Доставка недоступна',
                          'Работаем с ${loginProvide
                              .open} до ${loginProvide.close}', true);
                    }
                  }else{
                    if((loginProvide.currentOrderPrice + widget.price) >= loginProvide.curentMinPrice){
                      Provider.of<LoginProvide>(context, listen: false)
                          .order(loginProvide.user.token, context,true);
                    }else{
                      loginProvide.showLoseConnection(context, 'Внимание!', 'минимальная цена для доставки: ${loginProvide.curentMinPrice} тг', true);
                    }
                    print('minnnnn----------');
                    print(loginProvide.minPrices.last);}
                },
                child: Container(
                  height: 50,
                  decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor,
                  ),
                  child: Center(
                    child: !loginProvide.isOrdering ?  Text('Заказать за ${(loginProvide.currentOrderPrice + widget.price).toString() + ' тг'}',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontWeight: FontWeight.bold
                    ),) : LoadingCircle(),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class ChooseOption extends StatelessWidget {
  final text;
  final isWithMe;
  final myNumber;
  final onTap;
  ChooseOption({this.text,this.isWithMe,this.myNumber,this.onTap});
  @override
  Widget build(BuildContext context) {
    var secondaryTextStyle = TextStyle(
      color: Color(0xff0D0D0D),
      fontWeight: FontWeight.normal,
      fontSize: 12,
    );
    return Padding(
      padding: const EdgeInsets.only(left: 32),
      child: GestureDetector(
        onTap: onTap,
        child: Row(
          children: [
            Container(
              padding: EdgeInsets.all(2),
             width: 22,
              height: 22,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(100),
                  border: Border.all(width: 2, color: Theme.of(context).primaryColor)),
              child: myNumber == isWithMe ?  Container(
                width: 5,
                height: 5,
                decoration: BoxDecoration(
                color: Theme.of(context).primaryColor,
                    borderRadius: BorderRadius.circular(100),
                ),

              ) : SizedBox(),
            ),
            SizedBox(width: 16,),
            Text(text,style: secondaryTextStyle,),
          ],
        ),
      ),
    );
  }
}
Widget CardList(context, List<String> snap ) {
  return SingleChildScrollView(
    child: Card(
      borderOnForeground: false,
      child:
            ExpansionTile(
            onExpansionChanged: (val){
            },
            title: Text('ads',style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.w600,
                fontSize: 16
            ),),
            trailing: Icon( !true ? Icons.keyboard_arrow_down : Icons.keyboard_arrow_up,color: Colors.grey,size: 30,),
//            children: <Widget>[
//              ...snap,
//            ],
            children: snap.map((e) => ListTile(
              title: Text(e),
            )).toList(),
          ),

    ),
  );
}