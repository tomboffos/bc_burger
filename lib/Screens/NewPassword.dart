import 'package:bcburger/Screens/bottomNavBar.dart';
import 'package:bcburger/Screens/registration.dart';
import 'package:bcburger/Widgets/ActionButton.dart';
import 'package:bcburger/Widgets/CustomTextField.dart';
import 'package:bcburger/utils/loginProvider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
class NewPassword extends StatelessWidget {
  static var id = 'newPassword';
  @override
  Widget build(BuildContext context) {
    var h = MediaQuery.of(context).size.height;
    var loginProvide = Provider.of<LoginProvide>(context);
    var canTap = loginProvide.newPassword['code']!= null && loginProvide.newPassword['password']!= null &&  loginProvide.newPassword['password']!= '' && loginProvide.newPassword['password_confirmation']!= null && loginProvide.newPassword['password_confirmation']!= '';
    return Scaffold(
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(top:16.0),
              child: Text('Сброс пароля',style: TextStyle(color: Colors.black,fontSize: 20,fontWeight: FontWeight.bold),),
            ),
            Flexible(
              flex: 1,
              child: SizedBox(height: h,),
            ),
            SizedBox(height: 28,),
            CustomTextField(headText: 'Код',onChanged: (val){
              loginProvide.newPassword['code'] = val;
            },),
            SizedBox(height: 28,),
            CustomTextField(headText: 'Новый пароль',obscure: true,onChanged: (val){
              loginProvide.newPassword['password'] = val;
            },),
            SizedBox(height: 28,),
            CustomTextField(headText: 'Введите пароль повторно',obscure: true,onChanged: (val){
              loginProvide.newPassword['password_confirmation'] = val;
            },),
            SizedBox(height: 28,),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 32),
              child: ActionButton(text: 'Отправить письмо',enable:canTap,onTap: (){
                if(canTap){
                  loginProvide.resetPassword(context);
                }
              },trigger: loginProvide.forgotLoading,),
            ),
            Flexible(
              flex: 3,
              child: SizedBox(height: h,),
            ),
          ],
        ),
      ),
    );
  }
}

class RegisterButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var w = MediaQuery.of(context).size.width;
    var theme = Theme.of(context);
    return Container(
      width: w-32,
      padding:  EdgeInsets.symmetric(vertical: 16),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        border: Border.all(color: theme.primaryColor),
      ),
      child: Center(
        child: Text(
          'Регистрация',style: TextStyle(color: theme.primaryColor,fontWeight: FontWeight.bold,fontSize: 16),
        ),
      ),
    );
  }
}
