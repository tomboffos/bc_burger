class TopProducts {
  List<Data> data;

  TopProducts({this.data});

  TopProducts.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  int id;
  String name;
  String createdAt;
  List<TopProductsTop> products;

  Data({this.id, this.name, this.createdAt, this.products});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    createdAt = json['created_at'];
    if (json['products'] != null) {
      products = new List<TopProductsTop>();
      json['products'].forEach((v) {
        products.add(new TopProductsTop.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['created_at'] = this.createdAt;
    if (this.products != null) {
      data['products'] = this.products.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class TopProductsTop {
  int id;
  String image;
  String name;
  String path;
  String price;
  int catalogId;
  String edMassa;
  String content;
  String createdAt;
  String updatedAt;
  int fullscreen;

  TopProductsTop(
      {this.id,
        this.image,
        this.name,
        this.path,
        this.price,
        this.catalogId,
        this.edMassa,
        this.content,
        this.createdAt,
        this.updatedAt,
        this.fullscreen});

  TopProductsTop.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    image = json['images'];
    name = json['title'];
    path = json['path'];
    price = json['price'];
    catalogId = json['catalog_id'];
    edMassa = json['ed_massa'];
    content = json['content'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    fullscreen = json['fullscreen'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['images'] = this.image;
    data['title'] = this.name;
    data['path'] = this.path;
    data['price'] = this.price;
    data['catalog_id'] = this.catalogId;
    data['ed_massa'] = this.edMassa;
    data['content'] = this.content;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['fullscreen'] = this.fullscreen;
    return data;
  }
}