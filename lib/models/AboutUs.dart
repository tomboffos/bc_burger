class AboutUsModel {
  AboutInfo data;

  AboutUsModel({this.data});

  AboutUsModel.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new AboutInfo.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class AboutInfo {
  String title;
  String content;
  String heading;
  String subHeading;
  List<Addresses> addresses;

  AboutInfo({this.title, this.content, this.addresses});

  AboutInfo.fromJson(Map<String, dynamic> json) {
    title = json['title'];
    content = json['content'];
    heading = json['Heading'];
    subHeading = json['SubHeading'];
    if (json['addresses'] != null) {
      addresses = new List<Addresses>();
      json['addresses'].forEach((v) {
        addresses.add(new Addresses.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['title'] = this.title;
    data['SubHeading'] = this.subHeading;
    data['Heading']=this.heading;
    data['content'] = this.content;
    if (this.addresses != null) {
      data['addresses'] = this.addresses.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Addresses {
  String name;
  List<Items> items;

  Addresses({this.name, this.items});

  Addresses.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    if (json['items'] != null) {
      items = new List<Items>();
      json['items'].forEach((v) {
        items.add(new Items.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    if (this.items != null) {
      data['items'] = this.items.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Items {
  String name;
  String type;

  Items({this.name, this.type});

  Items.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    type = json['type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['type'] = this.type;
    return data;
  }
}