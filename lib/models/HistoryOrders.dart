class HistoryOrders {
  bool success;
  List<History> data;
  String message;

  HistoryOrders({this.success, this.data, this.message});

  HistoryOrders.fromJson(Map<String, dynamic> json) {
    // print(json['success']);

    success = json['success'];

    if (json['data'] != null) {
      data = new List<History>();
      json['data'].forEach((v) {

        data.add(new History.fromJson(v));
      });
    }


    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    data['message'] = this.message;
    return data;
  }
}

class History {
  String name;
  String address;
  String comment;
  String email;
  String phone;
  int price;
  String orderNumber;
  String status;
  String createdAt;
  List<ProductsHistory> products;

  History(
      {this.name,
        this.address,
        this.comment,
        this.email,
        this.phone,
        this.price,
        this.orderNumber,
        this.status,
        this.createdAt,
        this.products});

  History.fromJson(Map<String, dynamic> json) {
    print(json);
    name = json['title'];
    address = 'wdwd';
    comment = json['comment'];
    email = json['email'];
    phone = json['phone'];
    price = json['price'];
    orderNumber = json['order_number'];
    status = json['status'];
    createdAt = json['created_at'];


    if (json['products'] != null) {
      products = new List<ProductsHistory>();
      json['products'].forEach((v) {

        products.add(new ProductsHistory.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['address'] = this.address;
    data['comment'] = this.comment;
    data['email'] = this.email;
    data['phone'] = this.phone;
    data['price'] = this.price;
    data['order_number'] = this.orderNumber;
    data['status'] = this.status;
    data['created_at'] = this.createdAt;
    if (this.products != null) {
      data['products'] = this.products.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ProductsHistory {
  int id;
  String name;
  int price;
  int amount;
  int allPrice;
  String productId;
  String createdAt;

  ProductsHistory(
      {this.id,
        this.name,
        this.price,
        this.amount,
        this.allPrice,
        this.productId,
        this.createdAt});

  ProductsHistory.fromJson(Map<String, dynamic> json) {

    id = json['id'];
    name = json['title'];
    price = json['price'];
    amount = json['amount'];
    allPrice = json['allPrice'];
    productId = json['product_id'];
    createdAt = json['created_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.name;
    data['price'] = this.price;
    data['amount'] = this.amount;
    data['allPrice'] = this.allPrice;
    data['product_id'] = this.productId;
    data['created_at'] = this.createdAt;
    return data;
  }
}