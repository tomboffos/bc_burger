import 'package:bcburger/Screens/korzinaScreen.dart';
import 'package:flutter/material.dart';
class BusketIcon extends StatelessWidget {
  final int loginProvide;
  BusketIcon({this.loginProvide});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        Navigator.pushNamed(context, KorzinaScreen.id);
      },
      child: Container(
        width: 36,
        child: Stack(
          alignment: Alignment.centerLeft,
          children: [
            Positioned(
                child: Icon(Icons.shopping_cart,color: Colors.white,size: 28,)),
           if(loginProvide > 0) Positioned(
              top: 8,
              right: 0,
              child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(100),
                    color: Theme.of(context).primaryColor,
                ),
                padding: EdgeInsets.all(2),
                child: Container(
                  width: 14,
                    height: 14,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(100),
                      color: Colors.white
                    ),
                    child: Center(
                      child: Text(loginProvide.toString() ?? '',style: TextStyle(
                        color: Theme.of(context).primaryColor,
                        fontSize: 10,
                      ),),
                    )),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
