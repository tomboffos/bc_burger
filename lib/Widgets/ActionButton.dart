import 'package:bcburger/Widgets/LoadingCircle.dart';
import 'package:flutter/material.dart';
class ActionButton extends StatelessWidget {
  final text;
  final onTap;
  final enable;
  final trigger;
  ActionButton({this.text,this.onTap,this.enable,this.trigger});
  @override
  Widget build(BuildContext context) {
    var w = MediaQuery.of(context).size.width;
    var c = Theme.of(context);
    return Material(
      color: !enable ? c.buttonColor.withOpacity(0.5) : c.primaryColor,
      borderRadius:  BorderRadius.circular(8),
      child: InkWell(
        onTap: onTap,
        splashColor:Colors.white60,
        borderRadius: BorderRadius.circular(16),
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 16),
          width: w-32,
          child: (trigger ?? false) ? Center(child: LoadingCircle()) : Center(
            child:Text(
              text,
              style: TextStyle(
                  color: !enable ? Colors.black.withOpacity(0.7) : c.scaffoldBackgroundColor, fontSize: 16, fontWeight: FontWeight.bold),
            ),
          ),
        ),
      ),
    );
  }
}
