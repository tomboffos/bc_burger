import 'package:bcburger/Screens/bottomNavBar.dart';
import 'package:bcburger/utils/loginProvider.dart';
import 'package:flutter/material.dart';
import 'package:bcburger/Widgets/CustomTextField.dart';
import 'package:bcburger/Widgets/TwoTextField.dart';
import 'package:provider/provider.dart';
import 'ActionButton.dart';
class ProfileDataFields extends StatefulWidget {
  final saveOrNot;
  const ProfileDataFields({
    Key key,
    @required this.h,
    @required this.w,
    @required this.dropdownValue,
    @required this.items,
    this.saveOrNot,
    this.enable,
  }) : super(key: key);

  final double h;
  final enable;
  final double w;
  final String dropdownValue;
  final List<DropdownMenuItem<String>> items;

  @override
  _ProfileDataFieldsState createState() => _ProfileDataFieldsState();
}

class _ProfileDataFieldsState extends State<ProfileDataFields> {
  bool closed = false;
  @override
  Widget build(BuildContext context) {
    var loginProvide = Provider.of<LoginProvide>(context);
    return Container(
      height: widget.h-42,
      width: widget.w,
      child: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            CustomTextField(headText: 'Ваше имя',onChanged: (val){
              loginProvide.name = val;
            },),
            SizedBox(height: 28,),
            CustomTextField(headText: 'Телефон',onChanged: (val){
              loginProvide.tel = val;
            },),
            SizedBox(height: 28,),
            CustomTextField(headText: 'E-mail',onChanged: (val){
              loginProvide.email = val;
            },),
            SizedBox(height: 28,),
            CustomTextField(headText: 'Пароль',obscure: true,onChanged: (val){
              loginProvide.password = val;

            },),
            SizedBox(height: 28,),
            CustomTextField(headText: 'Введите пароль повторно',obscure: true,onChanged: (val){
              loginProvide.password2 = val;
            },),
            SizedBox(height: 28,),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 32),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Район доставки',
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 12,
                        fontWeight: FontWeight.w600
                    ),
                  ),
                  SizedBox(height: 8,),
                  SingleChildScrollView(
                    child: Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          border: Border.all(color: Colors.grey)
                      ),
                      child: ExpansionTile(
                        onExpansionChanged: (val){
                          setState(() {
                            closed = !closed;
                          });
                        },
                        title: Text((
                            loginProvide.raion == '' && loginProvide.raion ==
                                null ?
                            'Республики площадь, 4': loginProvide.raion) ?? ''
                            'Республики , 4',
                          style:
                        TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.normal,
                            fontSize: 16
                        ),),
                        trailing: Icon( closed ? Icons.arrow_drop_down : Icons.arrow_drop_up,color: Theme.of(context).primaryColor,size: 30,),
                        children: loginProvide.adressesArray.map((e) => Column(
                          children: [
                            Divider(thickness: 2,),
                            ListTile(
                              onTap: (){

                                loginProvide.chengeRaion(e);


                              },
                              title: Text(e),
                            ),
                          ],
                        )).toList(),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 28,),
            CustomTextField(headText: 'Адресс доставки',onChanged: (val){
              loginProvide.adrees = val;
            },),
            SizedBox(height: 28,),
            TwoTextField(w: widget.w),
            Padding(
              padding: const EdgeInsets.only(left: 32,right:32,top: 32,bottom: 64),
              child: ActionButton(trigger:loginProvide.isLoading, text:widget.saveOrNot ? 'Сохранить' : 'Зарегистрироваться',enable: widget.enable ?? true,onTap: (){
                if(widget.enable){
                  loginProvide.startLoading();
                  loginProvide.register(context);
                }
              },),
            ),
            SizedBox(height: 120,),
          ],
        ),
      ),
    );
  }
}
//loginProvide.chengeRaion(value);
