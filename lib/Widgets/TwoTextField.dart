import 'package:bcburger/utils/loginProvider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
class TwoTextField extends StatelessWidget {
  const TwoTextField({
    Key key,
    @required this.w,
  }) : super(key: key);

  final double w;

  @override
  Widget build(BuildContext context) {
    var loginProvide = Provider.of<LoginProvide>(context);
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 32),
      child: Row(children: [
        Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Номер квартиры',
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 12,
                    fontWeight: FontWeight.w600
                ),
              ),
              SizedBox(height: 8,),
              Container(
                width: w/2 -40,
                height: 50,
                child: TextField(
                  cursorColor: Colors.black,
                  decoration: new InputDecoration(
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.black),
                      borderRadius: new BorderRadius.circular(8.0),
                    ),
                    border: new OutlineInputBorder(
                      borderRadius: new BorderRadius.circular(8.0),
                      borderSide: new BorderSide(color:  Colors.black),
                    ),
                  ),
                  keyboardType: TextInputType.number,
                  style: new TextStyle(
                    color: Colors.black,
                  ),
                  onChanged: (val){
                    loginProvide.numberKV = val.toString();
                  },
                ),
              ),
            ],
          ),
        ),
        SizedBox(width: 16,),
        Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Подъезд',
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 12,
                    fontWeight: FontWeight.w600
                ),
              ),
              SizedBox(height: 8,),
              Container(
                width: w/2 -40,
                height: 50,
                child: TextField(
                  cursorColor: Colors.black,
                  decoration: new InputDecoration(
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.black),
                      borderRadius: new BorderRadius.circular(8.0),
                    ),
                    border: new OutlineInputBorder(
                      borderRadius: new BorderRadius.circular(8.0),
                      borderSide: new BorderSide(color:  Colors.black),
                    ),
                  ),
                  keyboardType: TextInputType.number,
                  style: new TextStyle(
                    color: Colors.black,
                  ),
                  onChanged: (val){
                    loginProvide.podyezd = val.toString();
                  },
                ),
              ),
            ],
          ),
        ),
      ],),
    );
  }
}
