import 'package:bcburger/Screens/ProductPage.dart';
import 'package:bcburger/Widgets/LikeButton.dart';
import 'package:bcburger/Widgets/LoadingCircle.dart';
import 'package:bcburger/models/user.dart';
import 'package:bcburger/utils/loginProvider.dart';
import 'package:bcburger/utils/productsProvider.dart';
import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:provider/provider.dart';
class SecondaryContainer extends StatelessWidget {
  final int id;
  final String image;
  final name;
  final content;
  final bool inFav;
  final gram;
  final price;
  SecondaryContainer({this.id,this.price,this.name,this.content,this.image,this.gram,this.inFav});
  @override
  Widget build(BuildContext context) {
    var productProvider =  Provider.of<LoginProvide>(context);
    var w = MediaQuery.of(context).size.width;
    User user = Provider.of<LoginProvide>(context).user;
    var mainTextStyle = TextStyle(
      fontWeight: FontWeight.w600,
      fontSize: 16,
    );
    var secondaryTextStyle = TextStyle(
      color: Color(0xff0D0D0D),
      fontWeight: FontWeight.normal,
      fontSize: 12,
    );
    return GestureDetector(
      onTap: (){
        Navigator.push(context, MaterialPageRoute(builder: (context)=> ProductPage(heroId: id,image: image ?? '',gram: gram ?? '',idItem: id,inFav: false,name: name ?? '',price: price ?? '',content: content ?? '',)));
        },
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 2,
              blurRadius: 7,
              offset: Offset(0, 3), // changes position of shadow
            ),
          ],
        ),
        width: w/2-20,
        height: 340,
        child: Stack(
          children: [
            Column(
              children: [
                Container(
                  width: w/2-12,
                  height: 120,
                  decoration:(image ?? false) != null ?   BoxDecoration(
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(8),topRight: Radius.circular(8)),
                    image: DecorationImage(
                        image:  CachedNetworkImageProvider(image,),fit: BoxFit.cover,
                  ),) : BoxDecoration(
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(8),topRight: Radius.circular(8)),
                  ),
                ),
                SizedBox(height: 8,),
                Text(name ?? '',style: mainTextStyle,textAlign: TextAlign.center,),
                SizedBox(height: 8,),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8),
                  child: Container(
                    width: w,
                    height: 1,
                    color: Theme.of(context).primaryColor,
                  ),
                ),
                SizedBox(height: 4,),
                LimitedBox(
                  maxHeight: 80,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 12),
                    child: SingleChildScrollView(child: Text(content ?? '',style: secondaryTextStyle,textAlign: TextAlign.center,)),
                  ),
                ),
                Expanded(
                  child: Container(
                    child: Column(children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 16,vertical: 4),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(gram ?? '',style: TextStyle(
                                fontSize: 12,
                                color: Color(0xff5E5E5E)
                            ),),
                            Text(price ?? '',style: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.bold,
                                color: Colors.black
                            ),),
                          ],
                        ),
                      ),
                      GestureDetector(
                        onTap: (){
                          productProvider.addToBusket(id,user.token,context);
                        },
                        child: Container(
                          width: w/2+32,
                          height: 50,
                          decoration: BoxDecoration(
                              color: Theme.of(context).primaryColor,
                              borderRadius: BorderRadius.only(bottomRight: Radius.circular(8),bottomLeft: Radius.circular(8))
                          ),
                          child: productProvider.isAddingItem != id ? Center(child: Text(
                            'В корзину',style: TextStyle(
                            color: Colors.white,
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                          ),
                          ),) : LoadingCircle(),
                        ),
                      ),
                    ],mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.end,
                    ),
                  ),
                )
              ],
            ),
          if((inFav ?? true) == true)  Positioned(top: 4,right: 8,
              child: LikeButton(id: id,)),
          ],
        ),
      ),
    );
  }
}
