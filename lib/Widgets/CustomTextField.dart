import 'package:flutter/material.dart';
class CustomTextField extends StatelessWidget {
  final hintText;
  final prefixIcon;
  final onChanged;
  final obscure;
  final textCtr;
  final headText;
  final keyBoard;

  CustomTextField({this.hintText, this.prefixIcon, this.obscure,this.onChanged,this.textCtr,this.headText, this.keyBoard});
  @override
  Widget build(BuildContext context) {
    var c = Theme.of(context);
    var w = MediaQuery.of(context).size.width;
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 32),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              headText,
              style: TextStyle(

                  color: Colors.black,
                  fontSize: 12,
                  fontWeight: FontWeight.w600
              ),
            ),
            SizedBox(height: 8,),
            Container(
              width: w -64,
              height: 50,
              child: TextField(
                controller: textCtr,
                cursorColor: Colors.black,
                obscureText: obscure ?? false,
                decoration: new InputDecoration(
                  hintText: hintText ?? '',
                  focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.black),
                    borderRadius: new BorderRadius.circular(8.0),
                  ),
                  border: new OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(8.0),
                    borderSide: new BorderSide(color:  Colors.black),
                  ),
                ),
                keyboardType:this.keyBoard != null ? this.keyBoard : TextInputType.emailAddress,
                style: new TextStyle(
                  color: Colors.black,
                ),
                onChanged: onChanged,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
