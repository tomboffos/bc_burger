import 'package:flutter/material.dart';
class LoadingCircle extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: SizedBox(
          width: 25,
          height: 25,
          child: Padding(
            padding: const EdgeInsets.all(4.0),
            child: CircularProgressIndicator(valueColor: new AlwaysStoppedAnimation<Color>(Colors.white)
            ),
          )),
    );
  }
}
