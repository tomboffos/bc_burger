import 'package:bcburger/Widgets/SecondaryContainer.dart';
import 'package:bcburger/models/FindOrders.dart';
import 'package:bcburger/utils/loginProvider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
class SearchButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return  GestureDetector(
      onTap: (){
        showSearch(context: context, delegate: Search(),);
      },
      child: Icon(
        Icons.search,
        color: Colors.white,
        size: 30,
      ),
    );
  }
}
class Search extends SearchDelegate  {
  @override
  void showResults(BuildContext context) {
    // TODO: implement showResults
    var token = Provider.of<LoginProvide>(context,listen: false).user.token;
    Provider.of<LoginProvide>(context,listen: false).findOrder(token, context,query);
  }
  @override
  // TODO: implement searchFieldLabel
  String get searchFieldLabel => 'Поиск';
  @override
  List<Widget> buildActions(BuildContext context) {
    return <Widget>[
      IconButton(
        icon: Icon(Icons.search),
        onPressed: () {
          var token = Provider.of<LoginProvide>(context,listen: false).user.token;
          Provider.of<LoginProvide>(context,listen: false).findOrder(token, context,query);
        },
      ),
      IconButton(
        icon: Icon(Icons.close),
        onPressed: () {
          query = "";
        },
      ),
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.arrow_back),
      onPressed: () {
        Navigator.pop(context);
      },
    );
  }

  String selectedResult = "";

  @override
  Widget buildResults(BuildContext context) {
    return Container(
      child: Center(
        child: Text(selectedResult),
      ),
    );
  }


  @override
  Widget buildSuggestions(BuildContext context) {
    return StreamBuilder<FindOrders>(
        stream: Provider.of<LoginProvide>(context).findController.stream.asBroadcastStream(),
        builder: (context, snapshot) {
          var w = MediaQuery.of(context).size.width;
          var h = MediaQuery.of(context).size.height;
          return  Padding(
            padding: const EdgeInsets.symmetric(vertical: 16,horizontal: 8),
            child: snapshot.data == null ? Center(child: Text('Начните поиск')) : snapshot.data.data.length == 0 ? Center(child: Text('Ничего не найдено'),) :  GridView.builder(gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisSpacing: 8,
                mainAxisSpacing: 24,
                childAspectRatio:(w/2-20)/340,
                crossAxisCount:  2 ), itemBuilder: (context, i){
              return SecondaryContainer(id: snapshot.data.data[i].id,name: snapshot.data.data[i].name,
                inFav: false,
                gram: snapshot.data.data[i].edMassa,
                image: snapshot.data.data[i].image,
                price: snapshot.data.data[i].price,
                content: snapshot.data.data[i].content,

              );
            },itemCount: snapshot.data.data.length,),
          );
        }
    );
  }
}
