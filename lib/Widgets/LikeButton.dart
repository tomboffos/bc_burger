import 'package:bcburger/StateManagement/provide.dart';
import 'package:bcburger/Widgets/OrangeCircle.dart';
import 'package:bcburger/models/Favourite.dart';
import 'package:bcburger/utils/loginProvider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
class LikeButton extends StatelessWidget {
  final int id;
  LikeButton({this.id,});
  @override
  Widget build(BuildContext context) {
    var token = Provider.of<LoginProvide>(context).user.token;
    var isAdding = Provider.of<LoginProvide>(context).addingFavItem;
    var allFav = Provider.of<LoginProvide>(context).allFavItems;
    List<FavouriteItem> hi = [];
    if(allFav.data.length == 0){
      hi = [];
    }else{
      hi = allFav.data.where((element) => (element.id ?? -1) == id).toList() ?? [];
    }
    return GestureDetector(
      onTap: (){
        if(!((hi?.length ?? 0) > 0)){
         Provider.of<LoginProvide>(context,listen: false).addFav(token, id, context);
        }else{
          Provider.of<LoginProvide>(context,listen: false).deleteFav(token, hi[0].favoritId,id ,context);
        }
      },
      child: isAdding == id ? OrangeCircle() :
      Icon((hi?.length ?? 0) > 0 ? Icons.favorite : Icons.favorite_border,color: Theme.of(context).primaryColor,size: 30,),
    );
  }
}
